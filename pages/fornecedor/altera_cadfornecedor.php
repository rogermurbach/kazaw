<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache" >

<head>
    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

    <script>
        function callconfirm(id)
        {
            bootbox.confirm({
                message: "TEM CERTEZA QUE DESEJA EXCLUIR ESTE CADASTRO?",
                buttons: {
                    confirm: {
                        label: 'SIM',
                        className: 'btn-success',
                        callback: function(){
                            window.location.href = "deleta_fornecedor.php"
                        }
                    },
                    cancel: {
                        label: 'NÃO',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                   // console.log('This was logged in the callback: ' + result);
                    if(result == true){
                        window.location.href = "deleta_fornecedor.php?id="+id
                    }else{

                    }

                }
            });

        }
    </script>


</head>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">
<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">
  <form class="form-horizontal" action="" method="POST">
    <?php
      if (isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
      }
      if (isset($_SESSION['msgcad'])){
        echo $_SESSION['msgcad'];
        unset($_SESSION['msgcad']);
      }
    ?>
<fieldset>

<!-- Form Name -->

<legend>Alterar Cadastro de Fornecedor</legend>

<?php
//include ("includes/config.php");

$qry = "select * from cad_fornecedores where cd_fornecedor=$_GET[id]";
$sql = mysqli_query($conn, $qry);
$info = mysqli_fetch_object($sql);



?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_fornecedor" type="text" value="<?php echo $info->cd_fornecedor; ?>" readonly="true" class="form-control input-md">
        </div>

  </div>


<!-- Text input-->
<div class="row">
  <label class="col-md-1 control-label" for="textinput">Nome/Razão*</label>
  <div class="col-md-5">
  <input id="textinput" name="nomerazao" type="text" value="<?php echo $info->for_nomerazao; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
</div>
<label class="col-md-1 control-label" for="textinput">Fantasia*</label>
  <div class="col-md-4">
  <input id="textinput" name="fantasia" type="text" value="<?php echo $info->for_fantasia; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
  </div>
  </div>

  <!-- Select Basic -->
  <div class="row">
    <label class="col-md-1 control-label" for="selectbasic">Grupo</label>
    <div class="col-md-5">
      <select id="selectbasic" name="tp_fornecedor" required="" class="form-control">
        <option label="- Selecione o tipo de pessoa -"></option>
        <option value="f" <?php if($info->tp_fornecedor == "f") { ?> selected <?php } ?>>Pessoa Física</option>
        <option value="j" <?php if($info->tp_fornecedor == "j") { ?> selected <?php } ?>>Pessoa Jurídica</option>
      </select>
    </div>
      </div>

  <div class="row">
    <label class="col-md-1 control-label" for="textinput">Fone 1</label>
    <div class="col-md-5">
    <input id="telefone" name="fone1" type="text" value="<?php echo $info->for_fone1; ?>" onfocus="javascript: retirarFormatacaoTel(this);" onblur="javascript: formatarCampoTel(this);" maxlength="14"  placeholder="DDD + Telefone" autocomplete="off" required="" class="form-control input-md">
  </div>
  <label class="col-md-1 control-label" for="textinput">Fone 2</label>
    <div class="col-md-4">
    <input id="telefone" name="fone2" type="text" value="<?php echo $info->for_fone2; ?>" onfocus="javascript: retirarFormatacaoTel(this);" onblur="javascript: formatarCampoTel(this);" maxlength="14" placeholder="DDD + Telefone" class="form-control input-md">
    </div>
    </div>

    <div class="row">
      <label class="col-md-1 control-label" for="textinput">Fone 3</label>
      <div class="col-md-5">
      <input id="telefone" name="fone3" type="text" value="<?php echo $info->for_fone3; ?>" onfocus="javascript: retirarFormatacaoTel(this);" onblur="javascript: formatarCampoTel(this);" maxlength="14" placeholder="DDD + Telefone" class="form-control input-md">
    </div>
    <label class="col-md-2 control-label" for="textinput">Data de abertura</label>
      <div class="col-md-3">
      <input id="textinput" name="nascimento" type="text" value="<?php echo $info->for_dtabertura; ?>" maxlength="10" onkeypress="mascaraData(this)" placeholder="" autocomplete="off" required="" class="form-control input-md">
      </div>
      </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Endereço</label>
        <div class="col-md-6">
        <input id="textinput" name="rua" type="text" value="<?php echo $info->for_endereco; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
      </div>
      <label class="col-md-1 control-label" for="textinput">Numero</label>
        <div class="col-md-3">
        <input id="textinput" name="numero" type="text" value="<?php echo $info->for_numero; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
        </div>
        </div>

        <div class="row">
          <label class="col-md-1 control-label" for="textinput">Bairro</label>
          <div class="col-md-5">
          <input id="textinput" name="bairro" type="text" value="<?php echo $info->for_bairro; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
        </div>
        <label class="col-md-1 control-label" for="textinput">Complem.</label>
          <div class="col-md-4">
          <input id="textinput" name="complemento" type="text" value="<?php echo $info->for_complemento; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
          </div>
          </div>

          <div class="row">
            <label class="col-md-1 control-label" for="textinput">Cidade</label>
            <div class="col-md-5">
            <input id="textinput" name="cidade" type="text" placeholder="" value="<?php echo $info->for_cidade; ?>" autocomplete="off" required="" class="form-control input-md">
          </div>
          <label class="col-md-1 control-label" for="selectbasic">Estado</label>
          <div class="col-md-1">
            <select id="selectbasic" name="estado" required=""class="form-control">
                <?php
                $sql2 =  mysqli_query($conn, "SELECT * FROM estados order by sigla asc");
                while ($result = mysqli_fetch_array($sql2) )
                {
                    ?>
                      <option value="<?php echo $result['id_estado'];?>" <?php if($result['id_estado'] == $info->for_estado){ echo 'selected'; } ?> ><?php echo $result['sigla']; ?></option>";
                    <?php
                }
                ?>
            </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">CEP</label>
            <div class="col-md-2">
            <input id="textinput" name="cep" type="text" value="<?php echo $info->for_cep; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
          </div>
            </div>

            <div class="row">
              <label class="col-md-1 control-label" for="textinput">Email</label>
              <div class="col-md-5">
              <input id="textinput" name="email" type="text" value="<?php echo $info->for_email; ?>" placeholder="" autocomplete="off" class="form-control input-md">
            </div>
              </div>

              <div class="row">
                <label class="col-md-1 control-label" for="textinput">CPF/CNPJ</label>
                <div class="col-md-5">
                <input id="textinput" name="cpfcnpj" type="text" value="<?php echo $info->for_cnpjcpf; ?>" onfocus="javascript: retirarFormatacaoCpfCnpj(this);" onblur="javascript: formatarCampoCpfCnpj(this);" maxlength="14" placeholder="" autocomplete="off" required="" class="form-control input-md">
              </div>
              <label class="col-md-1 control-label" for="textinput">Identidade</label>
                <div class="col-md-4">
                <input id="textinput" name="identidade" type="text" value="<?php echo $info->for_ident; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
                </div>
                </div>

                <div class="row">
                  <label class="col-md-1 control-label" for="textinput">Observação</label>
                  <div class="col-md-10">
                  <input id="textinput" name="obs" type="text" value="<?php echo $info->for_obs; ?>" placeholder="" autocomplete="off" class="form-control input-md">
                </div>
                  </div>

                  <div class="row">
                    <label class="col-md-1 control-label" for="textinput">Facebook</label>
                    <div class="col-md-9">
                    <input id="textinput" name="facebook" type="text" value="<?php echo $info->for_site1; ?>" placeholder="" autocomplete="off" class="form-control input-md">
                  </div>
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="button">Acessar</button>
                  </span>
                    </div>

                    <div class="row">
                      <label class="col-md-1 control-label" for="textinput">Instagram</label>
                      <div class="col-md-9">
                      <input id="textinput" name="instagram" type="text" value="<?php echo $info->for_site2; ?>" placeholder="" autocomplete="off" class="form-control input-md">
                    </div>
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Acessar</button>
                    </span>
                      </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" type="submit" name="gravar" value="Salvar"> </input> <button class="btn btn-secondary form-btn" type="button" onclick="callconfirm('<?php echo $info->cd_fornecedor ?>')" >Excluir </button> <input class="btn btn-danger form-btn" type="button" onclick="form.action='consulta_fornecedor.php'; form.submit()" value="Cancelar" >  </div>
                      </div>

</fieldset>
</form>

    <?php

    //include_once ("includes/config.php");

    if($_POST['gravar']){

        $codigo = $_POST["cd_fornecedor"];
        $nome = $_POST["nomerazao"];
        $fantasia = $_POST["fantasia"];
        $fone1 = $_POST["fone1"];
        $fone2 = $_POST["fone2"];
        $fone3 = $_POST["fone3"];
        $nascimento = $_POST["nascimento"];
        $rua = $_POST["rua"];
        $numero = $_POST["numero"];
        $bairro = $_POST["bairro"];
        $complemento = $_POST["complemento"];
        $cidade = $_POST["cidade"];
        $estado = $_POST["estado"];
        $cep = $_POST["cep"];
        $email = $_POST["email"];
        $cpfcnpj = $_POST["cpfcnpj"];
        $identidade = $_POST["identidade"];
        $observacao= $_POST["obs"];
        $site1 = $_POST["facebook"];
        $site2 = $_POST["instagram"];
        $tipo = $_POST['tp_fornecedor'];

        $nascimento = date("Y-m-d");
        $nascimento = strtotime($nascimento);

        $rotina = "ALTFOR";
        $codrotina = $codigo;
        $descricaolog = "Cadastro alterado de fornecedor - $nome";

        if (!mysqli_query($conn, "UPDATE cad_fornecedores SET for_nomerazao = '" . addslashes($nome) . "', for_fantasia = '" . addslashes($fantasia) . "',". 
        " for_fone1 = '" . $fone1 . "', for_fone2 = '" . $fone2 . "', for_fone3 = '" . $fone3 . "', for_dtabertura = '" . $nascimento . "', for_endereco = '" . addslashes($rua) . "', for_numero = '" . $numero . "',". 
        " for_bairro = '" . addslashes($bairro) . "', for_complemento = '" . addslashes($complemento) . "', for_cidade = '" . addslashes($cidade) . "', for_estado = '" . addslashes($estado) . "', for_cep = '" . $cep . "',". 
        " for_email = lower('$email'), for_cnpjcpf = '" . $cpfcnpj . "', for_ident = '" . $identidade . "', for_obs = '" . addslashes($observacao) . "', for_site1 = '" . addslashes($site1) . "',".
        " for_site2 = '" . addslashes($site2) . "', tp_fornecedor = '" . $tipo . "' WHERE cd_fornecedor = $codigo "))

         {
           echo("Erro: " . mysqli_error($conn));
           echo '<script type="text/javascript">toastr.error("Erro ao alterar Fornecedor!")</script>';
         }else{
           echo '<script type="text/javascript">toastr.success("Fornecedor alterado com sucesso!")</script>';
           $sql1 = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
           header("Location: altera_cadfornecedor.php?id=$codigo");
       }

    }

    ?>


</div>



</body>




</html>
