<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />





    <?php
    $titulo = "KAZA W";
    include_once ("includes/header.php");
    ?>

    <script>
        function callconfirm(cod)
        {
            bootbox.confirm({
                message: "TEM CERTEZA QUE DESEJA EXCLUIR ESTE ITEM?",
                buttons: {
                    confirm: {
                        label: 'SIM',
                        className: 'btn-success',
                        callback: function(){
                            window.location.href = "deleta_item_ajuste.php"
                        }
                    },
                    cancel: {
                        label: 'NÃO',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    // console.log('This was logged in the callback: ' + result);
                    if(result == true){
                        window.location.href = "deleta_item_ajuste.php?cod="+cod
                    }else{

                    }

                }
            });

        }
    </script>


    <script type="text/javascript">
        function Total(quantidade,ud,valortotal,value){
            quantidade=document.getElementById(quantidade);
            //value=document.getElementById(value);

            ud>0?quantidade.value++:quantidade.value--;
            quantidade.value=Math.max(quantidade.value,1);
            document.getElementById(valortotal).value=quantidade.value*value;
        }
    </script>

</head>


<body style="background-image:url(&quot;assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>





<?php
include('includes/topo_menu.php');
require_once('includes/config.php');
?>



<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="" method="post" name="venda">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
        <fieldset>

            <!-- Form Name -->

            <legend>Venda</legend>
            <hr>

            <?php

            $peganumitem = mysqli_query($conn, "SELECT iaju_numitem from itens_ajuste where cd_ajuest = $_GET[controle] UNION SELECT iaju_numitem from itens_ajuste order by iaju_numitem DESC limit 1");
            $resul = mysqli_fetch_array($peganumitem);


            $controle = $_GET['controle'];

            $carregaajuste = mysqli_query($conn, "select cd_ajuest, iaju_codprod as codigo, iaju_quantidade as quantidade, produtos.pro_descricao as produto FROM itens_ajuste INNER JOIN produtos ON itens_ajuste.iaju_codprod = produtos.cd_produto where cd_ajuest = $_GET[controle]");


            if($_GET['enviar']){

                $controle = $_GET['controle'];
                $cont = $controle;
                $dataajuste = $_GET['dataajuste'];
                $motivo = $_GET['motivo'];
                $dataajuste = date("Y-m-d",strtotime(str_replace('/','-',$dataajuste)));

                $qry = mysqli_query($conn, "INSERT INTO ajuste_estoque (aju_data, aju_motivo) values ('$dataajuste', '$motivo')");
            }


            $i = $_POST['nritem'];
            $i = $resul['iaju_numitem'] + 1;


            if($_POST['add']){

                $i = $_POST['nritem'];
                $i++;
                $nritem = $i;
                $controle = $_POST['controle'];

                $sqlcod = mysqli_query($conn, "SELECT cd_produto FROM produtos where pro_descricao like '$_POST[produto]'");
                $retcod = mysqli_fetch_array($sqlcod);
                $produto = $retcod['cd_produto'];
                $quantidade = $_POST['quantidade'];


                $qry2 = mysqli_query($conn, "INSERT INTO itens_ajuste (cd_ajuest, iaju_numitem, iaju_codprod, iaju_quantidade) values ('$controle', '$nritem', '$produto', '$quantidade')");

                $carregaajuste = mysqli_query($conn, "select cd_ajuest, iaju_codprod as codigo, iaju_quantidade as quantidade, produtos.pro_descricao as produto FROM itens_ajuste INNER JOIN produtos ON itens_ajuste.iaju_codprod = produtos.cd_produto where cd_ajuest = $controle");

                //echo $cont;

            }

            if($_POST['finalizar']){
                $rotina = "AJUEST";
                $codrotina = $_POST['controle'];
                $descricaolog = "Ajuste de estoque nº - '$codrotina'";
                $sqllog = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");

                $_SESSION['msg'] = "<BR><div class='alert alert-success'>Ajuste de estoque cadastrado com sucesso!</div>";
                header("Location: ajuste_estoque.php");
            }



            ?>

            <input type="hidden" name="controle" value="<?php echo $controle; ?>">
            <input type="hidden" name="nritem" value="<?php echo $i; ?>">


            <div class="row">
                <label class="col-md-2 control-label" for="selectbasic">Data</label>
                <div class="col-md-2">
                    <div class="input-group date">
                        <input type="text" class="form-control" id="dataentrada" name="dataajuste" >
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Cliente</label>
                <div class="col-md-7">
                    <input name="cliente" id="cliente" type="text" placeholder="Digite o nome do cliente" class="form-control input-md">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Observação</label>
                <div class="col-md-9">
                    <textarea name="obsvendacliente" type="text" placeholder="Observações gerais" rows="4" class="form-control input-md"></textarea>
                </div>
            </div>

            <hr>

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Ambiente</label>
                <div class="col-md-6">
                    <input name="obsvendacliente" type="text" placeholder="Digite a descrição do ambiente" class="form-control input-md">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Produto</label>
                <div class="col-md-6">
                    <input name="produto" id="produto" type="text" placeholder="Digite o nome do produto" class="form-control input-md">
                </div>
                <div>
                    <button class="btn btn-secondary form-btn" type="reset" href="">Adicionar Opcionais </button>

                </div>
            </div>
<input type="text" hidden value="10" id="valorun"/>

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Quantidade</label>
          <span class="input-group-btn">
              <input type="button" class="btn btn-light btn-number" data-type="minus" data-field="quant[1]" value="-" onclick='Total("quantidade",-1,"valortotal","valoruni");'>
                  <span class="glyphicon glyphicon-minus"></span>
              </input>
          </span>
                    <input type="text" name="quantidade" class="form-control input-number col-md-1" value="1" min="1" max="100" id="quantidade">
                    <span class="input-group-btn">
              <input type="button" class="btn btn-light btn-number" data-type="plus" data-field="quant[1]" value="+" onclick='Total("quantidade",1,"valortotal","valoruni");'>
                  <span class="glyphicon glyphicon-plus"></span>
              </input>
          </span>
                </div>

            <div class="row">

                <label class="col-md-2 control-label" for="textinput">Valor Unit.</label>
                <div class="col-md-2">
                    <input name="valoruni" id="valorunit" type="number" class="form-control input-md">
                </div>
                <label class="col-md-2 control-label" for="textinput">Valor Total</label>
                <div class="col-md-2">
                    <input name="valortotal" id="valortotal" readonly type="text" class="form-control input-md">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label">Medidas</label>
                <div class="col-md-3" style="padding-right: 0px; margin-left: 20px; top: 6px;">
                    <input name="medidas" id="chkmedidas" type="checkbox" class="form-check-input" value="" onclick="bloqueio()"/>
                </div>
            </div>

            <div class="row" id="medidasaplicavel" STYLE="display: none">
                <label class="col-md-2 control-label" for="textinput">A</label>
                    <input name="altura" type="text" value="0" class="col-md-2">
                <label class="col-md-1 control-label" for="textinput">L</label>
                    <input name="largura" type="text" value="0" class="col-md-2">
            </div>

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Observação</label>
                <div class="col-md-9">
                    <textarea name="obsvendaprod" type="text" placeholder="Observações do item" rows="3" class="form-control input-md"></textarea>
                </div>
            </div>


            <div id="cid_6" class="form-input-wide">
                <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                </div>
            </div>
            </li>


            <div class="form-row">
                <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="add" type="submit" value="Adicionar" > <button class="btn btn-danger form-btn" type="reset" href="">Limpar </button></div>
            </div>


        </fieldset>
    </form>


    <form class="form-horizontal" action="" method="post">

        <fieldset>

            <!-- Form Name -->
            <hr>
            <input type="hidden" name="controle" value="<?php echo $controle; ?>" />
            <input type="hidden" name="nritem" value="<?php echo $i; ?> ">



            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <td class="td" style="font-weight:bold; width: 10%;">Cód</td>
                    <td class="td" style="font-weight:bold; width: 55%;">Descrição</td>
                    <td class="td" style="font-weight:bold; width: 15%;">Medidas(LxA)</td>
                    <td class="td" style="font-weight:bold; width: 15%;">Quantidade</td>
                    <td class="td" style="font-weight:bold; width: 15%;">Valor Unit.</td>
                    <td class="td" style="font-weight:bold; width: 15%;">Valor Total</td>

                </tr>
                </thead>
                <tbody>
                <?php
                //$total = 0;
                while ($res = mysqli_fetch_array($carregaajuste))
                {
                    $codprod = $res['codigo'];
                    $estoque = mysqli_query($conn,"SELECT Sum(itens_entrada.ient_quantidade) as quantent FROM itens_entrada WHERE itens_entrada.ient_produto = '$codprod'");
                    $achaestoque = mysqli_fetch_array($estoque);
                    $saiestoque = mysqli_query($conn,"SELECT Sum(saida_produto.sai_quantidade) as quantsai FROM saida_produto WHERE saida_produto.sai_codprod = '$codprod");
                    $achasaiestoque = mysqli_fetch_array($saiestoque);
                    $ajustes = mysqli_query($conn,"SELECT Sum(ajuste_estoque.aju_quantidade) as quantaju, FROM ajuste_estoque WHERE ajuste_estoque.aju_codprod = '$codprod'");
                    $achaajuste = mysqli_fetch_array($ajustes);
                    $estocado = ($achaestoque['quantent'] - $achasaiestoque['quantsai']) + ($achaajuste['quantaju']);
                    ?>
                    <tr>
                        <td><input class='col-lg-9 form-control input-md' type='text' name='codigo' value="<?php echo $res['codigo']; ?>" /></td>
                        <td><input class='col-lg-12 form-control input-md' type='text' name='produto' value="<?php echo $res['produto']; ?>" /></td>
                        <td><input class='col-md-9 form-control input-md' type='text' name='quantidade' value="<?php echo $res['quantidade']; ?>" /></td>
                        <td><input class='col-md-9 form-control input-md' type='text' name='estoque' value="<?php echo $estocado; ?>" /></td>
                        <td><div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Opções
                                </button>
                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <a class="dropdown-item" name="excluiritem" onclick="callconfirm('<?php echo $res['codigo']; ?>')">Excluir</a>
                                </div>
                            </div></tr>
                <?php } ?>
                </tbody>
            </table>
            <br />

            <div class="form-row">
                <div class="col-md-12 content-right" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="finalizar" type="submit" value="Finalizar"></div>
            </div>

        </fieldset>
    </form>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>

</div>

<script type="text/javascript">
    $('#dataentrada').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>

<script type="text/javascript">
    $(function() {

        //autocomplete
        $("#produto").autocomplete({
            source: "INCLUDES/consprodvd.php",
            minLength: 1,
            select: function( event, ui ) {
                $("#valorunit").val(ui.item.valoruni);
            }
        });

    });
</script>

<script type="text/javascript">
    $(function() {

        //autocomplete
        $("#cliente").autocomplete({
            source: "INCLUDES/conscli.php",
            minLength: 3
        });

    });
</script>

<script  type="text/javascript">
    function bloqueio() {
        if (document.getElementById("medidasaplicavel").style.display == "none")
        {
            document.getElementById("medidasaplicavel").style.display = "block";
        }else{
            document.getElementById("medidasaplicavel").style.display = "none";
        }
    }
</script>

<script type="text/javascript">
    //plugin bootstrap minus and plus
    //http://jsfiddle.net/laelitenetwork/puJ6G/
    $('.btn-number').click(function(e){
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type      = $(this).attr('data-type');
        var input = $("input[name='"+fieldName+"']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if(type == 'minus') {

                if(currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if(parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if(type == 'plus') {

                if(currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if(parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });
    $('.input-number').focusin(function(){
        $(this).data('oldValue', $(this).val());
    });
    $('.input-number').change(function() {

        minValue =  parseInt($(this).attr('min'));
        maxValue =  parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if(valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if(valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });
    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });	</script>

</body>



</html>
