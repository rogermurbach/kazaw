<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="#" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Cadastro de Produto</legend>

    <?php

    $sql= mysqli_query($conn,"SELECT cd_produto FROM produtos order by cd_produto desc");
    $info = mysqli_fetch_array($sql);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-3 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_produto" type="text" readonly value="<?php $info = $info['cd_produto'] + 1; echo $info ?>" class="form-control input-md">
        </div>
          <label class="col-md-3 control-label" for="textinput">Data</label>
          <div class="col-md-2">
              <div class="input-group date">
                  <input type="text" class="form-control" id="datacadastro" name="datacadastro" >
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>
        </div>
  </div>

<!-- Text input-->
    <BR>
    <h4>Informações Gerais</h4>
    <hr>
<div class="row">
  <label class="col-md-1 control-label" for="textinput">Descrição</label>
  <div class="col-md-10">
  <input id="textinput" name="descricao" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
</div>
</div>

  <!-- Select Basic -->
  <div class="row">
    <label class="col-md-1 control-label" for="selectbasic">Grupo</label>
    <div class="col-md-5">
      <select id="selectbasic" name="grupo" required="" class="form-control">
          <option label="- Selecione o grupo do produto -">/option>
              <?php
              $sql2 =  mysqli_query($conn, "SELECT * FROM grupo_produtos order by gru_produto asc");
              while ($result = mysqli_fetch_array($sql2) )
              {
                  ?>
          <option value="<?php echo $result['cd_grupo']; ?>"><?php echo $result['gru_produto']; ?></option>";
          <?php
          }
          ?>

      </select>
    </div>
    <label class="col-md-2 control-label" for="textinput">Cód. Barras</label>
      <div class="col-md-3">
      <input id="textinput" name="codbarra" type="text" value="" placeholder="" autocomplete="off" class="form-control input-md">
      </div>
      </div>


  <div class="row">
    <label class="col-md-1 control-label" for="textinput">Unidade</label>
    <div class="col-md-2">
        <select id="selectbasic" name="unidade" required="" class="form-control">

            <?php
            $sql2 =  mysqli_query($conn, "SELECT * FROM unidade_produto order by uni_sigla asc");
            while ($result = mysqli_fetch_array($sql2) )
            {
                ?>
                <option value="<?php echo $result['cd_unidade']; ?>"><?php echo $result['uni_sigla']; ?></option>";
                <?php
            }
            ?>

        </select>
  </div>
    </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Acessórios</label>
            <a class="btn btn-primary" href="javascript:void(0)" id="addInput">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                Adicionar Acessório
            </a>
            <br/>

        </div>
        <div class="col-md-12 control-label" id="dynamicDiv">

        </div>

    <BR>
    <h4>Preços e Medidas</h4>
    <hr>
    <div class="row">
      <label class="col-md-2 control-label" for="textinput">Altura</label>
      <div class="col-md-3">
      <input id="telefone" name="altura" value="" type="text" maxlength="14" placeholder="" class="form-control input-md">
    </div>
    <label class="col-md-2 control-label" for="textinput">Largura</label>
      <div class="col-md-3">
      <input id="textinput" name="largura" value="" type="text" maxlength="10" placeholder="" autocomplete="off" class="form-control input-md">
      </div>
      </div>

      <div class="row">
        <label class="col-md-2 control-label" for="textinput">Preço Custo</label>
        <div class="col-md-3">
        <input id="textinput" name="precocusto" value="" type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
      </div>
      <label class="col-md-2 control-label" for="textinput">Preço Venda</label>
        <div class="col-md-3">
        <input id="textinput" name="precovenda" value="" type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
        </div>
        </div>

    <BR>
    <h4>Informações de Estoque</h4>
    <hr>

        <div class="row">
          <label class="col-md-2 control-label" for="textinput">Estoque Maximo</label>
          <div class="col-md-3">
          <input id="textinput" name="estoquemax" value="" type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
        </div>
        <label class="col-md-2 control-label" for="textinput">Estoque Minimo</label>
          <div class="col-md-3">
          <input id="textinput" name="estoquemin" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          </div>

          <div class="row">
            <label class="col-md-2 control-label" for="textinput">Estoque Atual</label>
            <div class="col-md-3">
            <input id="textinput" name="estoqueatual" value="" readonly type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
          </div>
            <label class="col-md-2 control-label" for="textinput">Obs</label>
            <div class="col-md-4">
                <textarea class="form-control input-md" type="text" rows="3" id="obs" name="obs"></textarea>
            </div>
            </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <button class="btn btn-danger form-btn" type="reset" href="#" onclick="abrirPag('clientes/consulta_cliente.php');">Cancelar </button></div>
                      </div>

</fieldset>
</form>

    <?php

    //include_once ("includes/config.php");

    $codigo = $_POST["cd_produto"];
    $datacad = $_POST["datacadastro"];
    $descricao = $_POST["descricao"];
    $grupo = $_POST["grupo"];
    $codbarra = $_POST["codbarra"];
    $unidade = $_POST["unidade"];
    $altura = $_POST["altura"];
    $largura = $_POST["largura"];
    $precoc = $_POST["precocusto"];
    $precov = $_POST["precovenda"];
    $estoquemin = $_POST["estoquemin"];
    $estoquemax = $_POST["estoquemax"];
    $estoqueatu = $_POST["estoqueatual"];
    $obs = $_POST["obs"];
    $acessorios = $_POST["acessorio"];

    $datacad = date("Y-m-d",strtotime(str_replace('/','-',$datacad)));

    if($_POST['gravar']){

        $rotina = "CADPROD";
        $codrotina = $codigo;
        $descricaolog = "Cadastro de produto - $descricao";


        $sql = mysqli_query($conn, "INSERT INTO produtos (pro_datacad, pro_descricao, pro_grupo, pro_codbarras, pro_unidade, pro_altura, pro_largura, pro_precocusto, pro_precovenda, pro_estoquemin, pro_estoquemax, pro_obs) VALUES('$datacad','$descricao','$grupo','$codbarra','$unidade','$altura','$largura','$precoc','$precov','$estoquemin','$estoquemax','$obs')");

        if ($sql) { // verificação para saber se foi cadastrado
            $_SESSION['msgcad'] = "<BR><div class='alert alert-success'>Produto cadastrado com sucesso!</div>";
            $sql1 = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
            header("Location: produtos.php");


        } else { // Caso dê erro
            $_SESSION['msg'] = "<BR><div class='alert alert-danger'>Erro ao cadastrar o produto!</div>"; mysqli_error($sql);
            $_SESSION["msgcad"] ="Não foi possível incluir " .mysqli_error($sql);
            header("Location: cadastro_produto.php");

        }


    }

    ?>


</div>
<script type="text/javascript">
    $('#datacadastro').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>

<script>
    $(function () {
        var scntDiv = $('#dynamicDiv');
        $(document).on('click', '#addInput', function () {
            $('<div id="rem" class="input-group" STYLE="padding-bottom: 10px; padding-left: 40px;">'+
                '<input class="form-control input-md" type="text" id="inputeste" size="20" value="" placeholder="" name="acessorio[]" /> '+
                '<a class="btn btn-danger form-btn" href="javascript:void(0)" id="remInput">'+
                '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> '+
                'Remover'+
                '</a>'+
            '</div>').appendTo(scntDiv);

            return false;
        });
        $(document).on('click', '#remInput', function () {
            $(this).parents('#rem').remove();
            return false;
        });
    });
</script>

</body>



</html>
