<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

    <script>
        function callconfirm(id)
        {
            bootbox.confirm({
                message: "TEM CERTEZA QUE DESEJA EXCLUIR ESTE CADASTRO?",
                buttons: {
                    confirm: {
                        label: 'SIM',
                        className: 'btn-success',
                        callback: function(){
                            window.location.href = "deleta_produto.php"
                        }
                    },
                    cancel: {
                        label: 'NÃO',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    // console.log('This was logged in the callback: ' + result);
                    if(result == true){
                        window.location.href = "deleta_produto.php?id="+id
                    }else{

                    }

                }
            });

        }
    </script>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Alterar Cadastro de Produto</legend>

    <?php

    $qry = "select * from produtos where cd_produto=$_GET[id]";
    $sql = mysqli_query($conn, $qry);
    $info = mysqli_fetch_object($sql);

    $datacadastro = $info->pro_datacad;

    $datacadastro = date("d/m/Y",strtotime(str_replace('-','/', $datacadastro)));

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-3 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_produto" type="text" readonly value="<?php echo $info->cd_produto; ?>" class="form-control input-md">
        </div>
          <label class="col-md-3 control-label" for="textinput">Data</label>
          <div class="col-md-2">
              <div class="input-group date">
                  <input type="text" class="form-control" id="datacadastro" name="datacadastro" readonly value="<?php echo $datacadastro ?>">
                  <div class="input-group-addon">
                      <span class="glyphicon glyphicon-th"></span>
                  </div>
              </div>
        </div>
  </div>

<!-- Text input-->
    <BR>
    <h4>Informações Gerais</h4>
    <hr>
<div class="row">
  <label class="col-md-1 control-label" for="textinput">Descrição</label>
  <div class="col-md-10">
  <input id="textinput" name="descricao" type="text" value="<?php echo $info->pro_descricao; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
</div>
</div>

  <!-- Select Basic -->
  <div class="row">
    <label class="col-md-1 control-label" for="selectbasic">Grupo</label>
    <div class="col-md-5">
      <select id="selectbasic" name="grupo" required="" class="form-control">
          <option label="- Selecione o grupo do produto -">/option>

              <?php
              $query = "select * from grupo_produtos";
              $results = mysqli_query($conn, $query);

              while ($rows = mysqli_fetch_array($results)){

              ?>
          <option <?php if($rows['cd_grupo']==$info->pro_grupo){echo " selected=selected";} ?> value="<?php echo $rows['cd_grupo'];?>"><?php echo $rows['gru_produto']; ?></option>

          <?php
          }
          ?>

      </select>
    </div>
    <label class="col-md-2 control-label" for="textinput">Cód. Barras</label>
      <div class="col-md-3">
      <input id="textinput" name="codbarra" type="text" value="<?php echo $info->pro_codbarras; ?>" placeholder="" autocomplete="off" class="form-control input-md">
      </div>
      </div>


  <div class="row">
    <label class="col-md-1 control-label" for="textinput">Unidade</label>
    <div class="col-md-1">
        <select id="selectbasic" name="unidade" required="" class="form-control">

            <?php
            $query = "select * from unidade_produto";
            $results = mysqli_query($conn, $query);

            while ($rows = mysqli_fetch_array($results)){
                ?>
                <option <?php if($rows['cd_unidade']==$info->pro_unidade){echo " selected=selected";} ?> value="<?php echo $rows['cd_unidade'];?>"><?php echo $rows['uni_sigla']; ?></option>

                <?php
            }
            ?>

        </select>
  </div>
    </div>
    <BR>
    <h4>Preços e Medidas</h4>
    <hr>
    <div class="row">
      <label class="col-md-2 control-label" for="textinput">Altura</label>
      <div class="col-md-3">
      <input id="telefone" name="altura" value="<?php echo $info->pro_altura; ?>" type="text" maxlength="14" placeholder="" class="form-control input-md">
    </div>
    <label class="col-md-2 control-label" for="textinput">Largura</label>
      <div class="col-md-3">
      <input id="textinput" name="largura" value="<?php echo $info->pro_largura; ?>" type="text" maxlength="10" placeholder="" autocomplete="off" class="form-control input-md">
      </div>
      </div>

      <div class="row">
        <label class="col-md-2 control-label" for="textinput">Preço Custo</label>
        <div class="col-md-3">
        <input id="textinput" name="precocusto" value="<?php echo $info->pro_precocusto; ?>" type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
      </div>
      <label class="col-md-2 control-label" for="textinput">Preço Venda</label>
        <div class="col-md-3">
        <input id="textinput" name="precovenda" value="<?php echo $info->pro_precovenda; ?>" type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
        </div>
        </div>

    <BR>
    <h4>Informações de Estoque</h4>
    <hr>

        <div class="row">
          <label class="col-md-2 control-label" for="textinput">Estoque Maximo</label>
          <div class="col-md-3">
          <input id="textinput" name="estoquemax" value="<?php echo $info->pro_estoquemax; ?>" type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
        </div>
        <label class="col-md-2 control-label" for="textinput">Estoque Minimo</label>
          <div class="col-md-3">
          <input id="textinput" name="estoquemin" value="<?php echo $info->pro_estoquemin; ?>" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          </div>

          <div class="row">
            <label class="col-md-2 control-label" for="textinput">Estoque Atual</label>
            <div class="col-md-3">
            <input id="textinput" name="estoqueatual" value="<?php echo $info->pro_estoque; ?>" readonly type="text" placeholder="" autocomplete="off" required="" class="form-control input-md">
          </div>
            <label class="col-md-2 control-label" for="textinput">Obs</label>
            <div class="form-group">
                <textarea class="form-control" rows="3" id="obs" name="obs" value="<?php echo $info->pro_obs; ?>"></textarea>
            </div>
            </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


        <div class="form-row">
            <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" type="submit" name="gravar" value="Salvar"> </input> <button class="btn btn-secondary form-btn" type="button" onclick="callconfirm('<?php echo $info->cd_produto ?>')" >Excluir </button> <input class="btn btn-danger form-btn" type="button" onclick="form.action='produtos.php'; form.submit()" value="Cancelar" >  </div>
        </div>

</fieldset>
</form>

    <?php

    //include_once ("includes/config.php");

    $codigo = $_POST["cd_produto"];
    $datacad = $_POST["datacadastro"];
    $descricao = $_POST["descricao"];
    $grupo = $_POST["grupo"];
    $codbarra = $_POST["codbarra"];
    $unidade = $_POST["unidade"];
    $altura = $_POST["altura"];
    $largura = $_POST["largura"];
    $precoc = $_POST["precocusto"];
    $precov = $_POST["precovenda"];
    $estoquemin = $_POST["estoquemin"];
    $estoquemax = $_POST["estoquemax"];
    $estoqueatu = $_POST["estoqueatual"];
    $obs = $_POST["obs"];

    $datacad = date("Y-m-d",strtotime(str_replace('/','-',$datacad)));

    $rotina = "ALTPROD";
    $codrotina = $codigo;
    $descricaolog = "Alteração de produto - $descricao";

    if($_POST['gravar']){

        $sql = mysqli_query($conn, "UPDATE produtos SET pro_datacad = '$datacad', pro_descricao = '$descricao', pro_grupo = '$grupo', pro_codbarras = '$codbarra', pro_unidade = '$unidade', pro_altura = '$altura', pro_largura = '$largura', pro_precocusto = '$precoc', pro_precovenda = '$precov', pro_estoquemin = '$estoquemin', pro_estoquemax = '$estoquemax', pro_obs = '$obs' where cd_produto = $codigo");

        if ($sql) { // verificação para saber se foi cadastrado
            $_SESSION['msgcad'] = "<BR><div class='alert alert-success'>Produto alterado com sucesso!</div>";
            $sql1 = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
            header("Location: altera_cadproduto.php?id=$codigo");


        } else { // Caso dê erro
            $_SESSION['msg'] = "<BR><div class='alert alert-danger'>Erro ao alterar o produto!</div>"; mysqli_error($sql);
            //$_SESSION["msgcad"] ="Não foi possível incluir " .mysqli_error($sql);
            header("Location: altera_cadproduto.php?id=$codigo");

        }


    }

    ?>


</div>


</body>



</html>
