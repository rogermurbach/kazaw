<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <script type="text/javascript">
        function id( el ){
            return document.getElementById( el );
        }
        window.onload = function(){
            id('mais').onclick = function(){
                id('quantidade').value = parseInt( id('quantidade').value )+1;

                id('total').value = id('valor')*id('quantidade').value;
            }
            id('menos').onclick = function(){
                if( id('quantidade').value>0 )
                    id('quantidade').value = parseInt( id('quantidade').value )-1;

                id('total').value = id('valor')*id('quantidade').value;
            }
        }
    </script>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
    function soNumero($str) {
        return preg_replace("/[^0-9]/", "", $str);
    }
</script>



<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

        <?php // essa pagina faz parte do cadastro de entrada de estoque


        if($_POST['enviar'])
        {
            $fornecedor = $_POST['fornecedor'];
            $numeronf = $_POST['nf'];
            $valornf = $_POST['valornf'];
            $data = $_POST['datanf'];
            $data = date("Y-m-d",strtotime(str_replace('/','-',$data)));
            $cadastra = mysqli_query($conn,"INSERT INTO entrada_produto (ent_nf, ent_data, ent_valornf, ent_fornecedor) values ('$numeronf','$data','$valornf','$fornecedor')");
        }
        $sql3= mysqli_query($conn,"SELECT cd_entrada FROM entrada_produto order by cd_entrada desc");
        $controlee = mysqli_fetch_array($sql3);
        $controle = $controlee['cd_entrada'];
        $i = $_POST['nritem'];


        if ($_POST['add'])
        {

            $sqlcod = mysqli_query($conn, "SELECT cd_produto FROM produtos where pro_descricao like '$_POST[produto]'");
            $retcod = mysqli_fetch_array($sqlcod);
            $produto = $retcod['cd_produto'];

            $i = $_POST['nritem'];
            $i++;
            $nritem = $i;
            $controle = $_POST['controle'];
            $quant = $_POST['quantidade'];
            //$produto = $_POST['produto'];
            //$produtofil = preg_replace("/[^0-9]/", "",$produto);
            $custoun = $_POST['custoun'];
            $custoprod = mysqli_query($conn,"SELECT * FROM produtos WHERE cd_produto = '$produto' ");
            $achacusto = mysqli_fetch_array($custoprod);
            $custocad = $achacusto['pro_precocusto'];
            $customed = $achacusto['pro_customed'];
            if($custoun != $custocad)
            {
                $customed = ($customed + $custoun) / 2;
                $altera = mysqli_query($conn,"UPDATE produtos SET pro_precocusto='$custoun', pro_customed='$customed' WHERE cd_produto = '$produto'");
            }
            $cad = mysqli_query($conn,"INSERT INTO itens_entrada (cd_itement, ient_cditem, ient_produto, ient_quantidade, ient_vcusto) values ('$controle','$nritem','$produto','$quant','$custoun')") or die (mysqli_error($conn));
            $achaprod = mysqli_query($conn,"SELECT itens_entrada.cd_itement, itens_entrada.ient_cditem, itens_entrada.ient_produto, produtos.pro_descricao as produtos, produtos.cd_produto as codigo, itens_entrada.ient_quantidade as quantidade, itens_entrada.ient_vcusto as custo FROM itens_entrada INNER JOIN produtos ON itens_entrada.ient_produto = produtos.cd_produto WHERE itens_entrada.cd_itement = '$controle'") or die (mysqli_error($conn));
        }
        ?>

    <form class="form-horizontal" action="" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
        <fieldset>

            <!-- Form Name -->

            <legend>Selecione o Produto</legend>

            <div id="cid_6" class="form-input-wide">
                <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
                </div>
            </div>


            <input type="hidden" name="controle" value="<?php echo $controle; ?>">
            <input type="hidden" name="nritem" value="<?php echo $i; ?>">

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Produto</label>
                <div class="col-md-5">
                    <input name="produto" id="produto" type="text" placeholder="Digite o nome do produto" class="form-control input-md">
                </div>
            </div>

            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Quantidade</label>
                <div>
                    <input type="button" class="btn btn-light" name="menos" id="menos" value="-">
                </div>
                <div class="col-md-1" style="padding-right: 0px">
                    <input name="quantidade" id="quantidade" value="0" type="text" class="form-control input-md">
                </div>
                <div>
                    <input type="button" class="btn btn-light" name="mais" id="mais" value="+">
                </div>
            </div>


            <div class="row">
                <label class="col-md-2 control-label" for="textinput">Custo (un)</label>
                <div class="col-md-2">
                    <input id="textinput" name="custoun" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
                </div>
            </div>

            <div id="cid_6" class="form-input-wide">
                <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                </div>
            </div>
            </li>


            <div class="form-row">
                <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="add" type="submit" value="Adicionar" > <button class="btn btn-danger form-btn" type="reset" href="">Limpar </button></div>
            </div>

        </fieldset>
    </form>

    <form class="form-horizontal" action="condicao_pagamento.php" method="post">

        <fieldset>

            <!-- Form Name -->
            <legend><center>Pedido de Entrada Nº <?php echo $controle ?></center></legend>
            <hr>
            <input type="hidden" name="controle" value="<?php echo $controle; ?>" />

        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr>
                <td class="td" style="font-weight:bold;">Produto</td>
                <td width="10%" class="td" style="font-weight:bold;">Quantidade</td>
                <td class="td" style="font-weight:bold;">Custo</td>
                <td class="td" style="font-weight:bold;">Custo Total</td>
                <td class="td" style="font-weight:bold;">No Estoque</td>
            </tr>
            <?php
            $total = 0;
            while ($result = mysqli_fetch_array($achaprod))
            {
                $custototal = $result['quantidade'] * $result['custo'];
                $total = $total + $custototal;

                $codprod = $result['codigo'];
                $estoque = mysqli_query($conn,"SELECT Sum(itens_entrada.ient_quantidade) as quantent FROM itens_entrada WHERE itens_entrada.ient_produto = '$codprod'");
                $achaestoque = mysqli_fetch_array($estoque);
                $saiestoque = mysqli_query($conn,"SELECT Sum(sitens.QUANT) as quantsai FROM sitens WHERE sitens.PRODUTO = '$codprod");
                $achasaiestoque = mysqli_fetch_array($saiestoque);
                $ajustes = mysqli_query($conn,"SELECT Sum(ajuste.qtde_saida) as quantsai, Sum(ajuste.qtde_entrada) as quantent FROM ajuste WHERE ajuste.PRODUTO = '$codprod'");
                $achaajuste = mysqli_fetch_array($ajustes);
                $estocado = ($achaestoque['quantent'] - $achasaiestoque['quantsai']) + ($achaajuste['quantent'] - $achaajuste['quantsai']);
                echo "<tr>
											<td><input class='col-lg-12 form-control input-md' type='text' name='produto' value='".$result['produtos']."' /></td>
											<td><input class='col-md-9 form-control input-md' type='text' name='quantidade' value='".$result['quantidade']."' /></td>
											<td><input class='col-md-9 form-control input-md' type='text' name='custo' value='".number_format($result['custo'], 2, ',', '.')."' /></td>
											<td><input class='col-md-9 form-control input-md' type='text' name='custototal' value='".number_format($custototal, 2, ',', '.')."' /></td>
											<td><input class='col-md-9 form-control input-md' type='text' name='estoque' value='".$estocado."' /></td>
											<td><div class=\"btn-group\" role=\"group\">
                                                    <button id=\"btnGroupDrop1\" type=\"button\" class=\"btn btn-secondary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                        Opções
                                                    </button>
                                                    <div class=\"dropdown-menu\" aria-labelledby=\"btnGroupDrop1\">
                                                        <a class=\"dropdown-item\" href=\"#\">Excluir</a>
                                                        <a class=\"dropdown-item\" href=\"#\">Alterar</a>
                                                    </div>
                                            </div></tr>
										</tr>
										";
            }
            echo "
									<tr><td></td><td></td>
										<td class='td' style='font-weight:bold;'>Total Ped:</td>
										<td style='background: #EEE;'><input readonly class='quant' type='text' name='total' value='R$".number_format($total, 2, ',', '.')."' /></td>
									</tr>
									";
            ?>
            </tbody>
        </table>
        <br />

        <div class="form-row">
            <div class="col-md-12 content-right" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="finalizar" type="submit" value="Finalizar"></div>
        </div>

    </fieldset>
    </form>
  <!--  <form method="post" action="realiza_entrada_produto.php">
        <input type="hidden" name="controle" value="<?php// echo $controle; ?>" />
        <input type="hidden" name="nritem" value="<?php //echo $i; ?>" />
        <div class="form-row">
            <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-danger form-btn" type="submit" name="maisitens" value="Adicionar + Produtos"></input></div>
        </div>
</form> -->

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>

</div>

<script type="text/javascript">
    $('#dataentrada').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>

<script type="text/javascript">
    $(function() {

        //autocomplete
        $("#produto").autocomplete({
            source: "../../INCLUDES/consprod.php",
            minLength: 1
        });

    });
</script>


</body>



</html>
