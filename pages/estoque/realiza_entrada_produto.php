<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <script type="text/javascript">
        function id( el ){
            return document.getElementById( el );
        }
        window.onload = function(){
            id('mais').onclick = function(){
                id('quantidade').value = parseInt( id('quantidade').value )+1;

                id('total').value = id('valor')*id('quantidade').value;
            }
            id('menos').onclick = function(){
                if( id('quantidade').value>0 )
                    id('quantidade').value = parseInt( id('quantidade').value )-1;

                id('total').value = id('valor')*id('quantidade').value;
            }
        }
    </script>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <?php // essa pagina faz parte do cadastro de entrada de estoque
    //include_once ('includes/config.php');

    if($_POST['enviar'])
    {
        $fornecedor = $_POST['fornecedor'];
        $numeronf = $_POST['nf'];
        $valornf = $_POST['valornf'];
        $data = $_POST['datanf'];
        $data = date("Y-m-d",strtotime(str_replace('/','-',$data)));
        $cadastra = mysqli_query($conn,"INSERT INTO entrada_produto (ent_nf, ent_data, ent_valornf, ent_fornecedor) values ('$numeronf','$data','$valornf','$fornecedor')");
    }
    $sql3= mysqli_query($conn,"SELECT cd_entrada FROM entrada_produto order by cd_entrada desc");
    $controlee = mysqli_fetch_array($sql3);
    $controle = $controlee['cd_entrada'];
    $i = $_POST['nritem'];




    ?>


    <form class="form-horizontal" action="finaliza_entrada_produto.php" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend><center>Selecione o Produto</center></legend>

        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>


        <input type="hidden" name="controle" value="<?php echo $controle; ?>">
        <input type="hidden" name="nritem" value="<?php echo $i; ?>">

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Produto</label>
            <div class="col-md-5">
                <input name="produto" id="produto" type="text" placeholder="Digite o nome do produto" class="form-control input-md">
            </div>
        </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Quantidade</label>
            <div>
                <input type="button" class="btn btn-light" name="menos" id="menos" value="-">
            </div>
            <div class="col-md-1" style="padding-right: 0px">
                <input name="quantidade" id="quantidade" value="0" type="text" class="form-control input-md">
            </div>
            <div>
                <input type="button" class="btn btn-light" name="mais" id="mais" value="+">
            </div>
        </div>


        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Custo (un)</label>
            <div class="col-md-2">
                <input id="textinput" name="custoun" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
            </div>
        </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="add" type="submit" value="Adicionar" > <button class="btn btn-danger form-btn" type="reset" href="">Limpar </button></div>
                      </div>

</fieldset>
</form>

    <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.1/jquery-ui.min.js"></script>

</div>

<script type="text/javascript">
    $('#dataentrada').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>

<script type="text/javascript">
    $(function() {

        //autocomplete
        $("#produto").autocomplete({
            source: "INCLUDES/consprod.php",
            minLength: 1
        });

    });
</script>



</body>



</html>
