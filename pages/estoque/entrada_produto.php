<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="finaliza_entrada_produto.php" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Entrada de Produtos</legend>

    <?php

    $sql= mysqli_query($conn,"SELECT for_nomerazao, cd_fornecedor FROM cad_fornecedores order by for_nomerazao asc");
    $sql2 = mysqli_query($conn,"SELECT cd_entrada FROM entrada_produto order by cd_entrada desc");
    $info = mysqli_fetch_array($sql2);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_entrada" type="text" readonly value="<?php $info = $info['cd_entrada'] + 1; echo $info ?>" class="form-control input-md">
        </div>
  </div>
        <div class="row">
            <label class="col-md-1 control-label" for="selectbasic">Fornecedor</label>
            <div class="col-md-5">
                <select id="selectbasic" name="fornecedor" class="form-control">
                    <option label="Selecione o fornecedor"></option>

                    <?php
                    while ($result = mysqli_fetch_array($sql) )
                    {
                        ?>
                        <option value="<?php echo $result['cd_fornecedor']; ?>"><?php echo $result['for_nomerazao']; ?></option>";
                        <?php
                    }
                    ?>

                </select>
            </div>
        </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Nº NF</label>
            <div class="col-md-3">
                <input id="telefone" name="nf" value="" type="text" maxlength="14" placeholder="" class="form-control input-md">
            </div>
        </div>

        <div class="row">
            <label class="col-md-1 control-label" for="selectbasic">Data</label>
            <div class="col-md-2">
                <div class="input-group date">
                    <input type="text" class="form-control" id="dataentrada" name="datanf" >
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Valor NF</label>
            <div class="col-md-2">
                <input id="textinput" name="valornf" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
            </div>
        </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="enviar" type="submit" value="Continuar" /> <button class="btn btn-danger form-btn" type="reset" href="">Limpar </button></div>
                      </div>
</fieldset>
</form>

</div>

<script type="text/javascript">
    $('#dataentrada').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>


</body>



</html>
