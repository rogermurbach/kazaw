<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" />


    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>


<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="#" method="post">



        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend><center>Resumo do Pedido</center></legend>
        <hr>


        <?php
        $sqlentra1 = mysqli_query($conn,"SELECT *, date_format(ent_data, '%d/%m/%Y') AS data FROM entrada_produto ORDER BY cd_entrada")or die(mysqli_error($conn));
        $fornec = mysqli_fetch_array($sqlentra1);

        if($_GET)//mostra detalhadamente
        {
            $cod = $_GET['id'];
            $sqlentra = mysqli_query($conn,"SELECT *, date_format(ent_data, '%d/%m/%Y') AS data FROM entrada_produto WHERE cd_entrada = '$cod' order by cd_entrada desc")or die(mysqli_error($conn));
            $result = mysqli_fetch_array($sqlentra);
            $fornec = $result['ent_fornecedor'];
            $sqlforn = mysqli_query($conn,"SELECT for_nomerazao, cd_fornecedor FROM cad_fornecedores WHERE cd_fornecedor = '$fornec'")or die(mysqli_error($conn));
            $achafor = mysqli_fetch_array($sqlforn);

            $rotina = "ENTPED";
            $codrotina = $cod;
            $rotinanf = $result['ent_nf'];
            $descricaolog = "Pedido de entrada nº - $rotinanf";
            $sqllog = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");


            ?>
            <table class="table table-striped table-bordered table-hover" align="center">
                <h5>Fornecedor</h5>
                <tbody>
                <tr>
                    <td class='td' style="font-weight:bold;" width="60">Cód</td>
                    <td class='td' style="font-weight:bold;" width="60">Data</td>
                    <td class='td' style="font-weight:bold;" width="200">Fornecedor</td>
                    <td class='td' style="font-weight:bold;" width="70">Nota Fiscal</td>
                    <td class='td' style="font-weight:bold;" width="60">Valor Nota Fiscal</td>

                </tr>
                <?php
                $valornf = number_format($result['ent_valornf'], 2, ',', '.');
                echo "<tr>";
                echo "<td class='td'>".$result['cd_entrada']."</td>";
                echo "<td class='td'>".$result['ent_data']."</td>";
                echo "<td class='td'>".$achafor['for_nomerazao']."</td>";
                echo "<td class='td'>".$result['ent_nf']."</td>";
                echo "<td class='preco'>R$ ".$valornf."</td>";
                echo "</tr>";
                ?>
                </tbody>
            </table><br />
            <table class="table table-striped table-bordered table-hover" align="center">
                <h5>Detalhes de produtos</h5>
                <tbody>
                <tr>
                    <td class='td' style="font-weight:bold;"width="auto">Item</td>
                    <td class='td' style="font-weight:bold;"width="auto">Produto</td>
                    <td class='td' style="font-weight:bold;"width="auto">Quantidade</td>
                    <td class='td' style="font-weight:bold;"width="auto">Custo Unit&aacute;rio</td>
                    <td class='td' style="font-weight:bold;"width="auto">Custo Total</td>
                </tr>

                <?php
                $itens = mysqli_query($conn,"SELECT * FROM itens_entrada WHERE cd_itement = '$cod' order by ient_cditem")or die (mysqli_error($conn));
                while($result = mysqli_fetch_array($itens))//carrega os dados escolhidos no get
                {
                    $codprod = $result['ient_produto'];
                    $produto = mysqli_query($conn,"SELECT pro_descricao, cd_produto FROM produtos WHERE cd_produto = '$codprod'")or die (mysqli_error($conn));;
                    $prod = mysqli_fetch_array($produto);
                    $totalprod = $result['ient_vcusto'] * $result['ient_quantidade'];
                    $total = $total + $totalprod;
                    $totalprod = number_format($totalprod, 2, ',', '.');
                    $custo = number_format($result['ient_vcusto'], 2, ',', '.');
                    echo "<tr>";
                    echo "<td class='td'>".$result['ient_cditem']."</td>";
                    echo "<td class='td'>".$prod['pro_descricao']."</td>";
                    echo "<td class='preco'>".$result['ient_quantidade']."</td>";
                    echo "<td class='preco'>R$ ".$custo."</td>";
                    echo "<td class='preco'>R$ ".$totalprod."</td>";
                    echo "</tr>";
                }
                ?>		<tr><td></td><td></td><td></td><td class='preco' style="font-weight:bold;">Total</td><td class='preco' style="font-weight:bold;">R$ <?php echo number_format($total, 2, ',', '.'); ?></td></tr>
                </tbody>
            </table><br />

            <table class="table table-striped table-bordered table-hover" align="center">
                <h5>Detalhes de pagamento</h5>
                <tbody>
                <tr>
                    <td class="td" style="font-weight:bold;">Duplicata</td>
                    <td class="td" style="font-weight:bold;">Vencimento</td>
                    <td class="td" style="font-weight:bold;">Valor</td>
                </tr>

                <?php
                $pagamento = mysqli_query($conn, "SELECT * FROM contas_pagar where conpag_controle = '$cod'");
                While($res = mysqli_fetch_array($pagamento))
                {
                    $duplicata = $res['conpag_duplicata'];
                    $vencimento = $res['conpag_datavecto'];
                    $valor = $res['conpag_valor'];

                    echo "<tr>";
                    echo "<td>".$res['conpag_duplicata']."</td>";
                    echo "<td>".$res['conpag_datavecto']."</td>";
                    echo "<td>".$res['conpag_valor']."</td>";
                    echo "</tr>";
                }



                ?>

                </tbody>
            </table>
            <div class="form-row">
            <input class="btn btn-primary form-btn" type="button" href="#materia" onclick="window.print()" value="Imprimir"><BR>
            <a href="entrada_produto.php"><input class="btn btn-primary form-btn" type="button" name="voltar" value="Finalizar" ></a>
            </div>
            <BR>
            <?php
        }// fim do if
        else//começa o form e os resultados
        {
        $sql2= mysqli_query($conn,"SELECT for_nomerazao, cd_fornecedor FROM cad_fornecedores order by for_nomerazao") or die ("Erro");
        ?>
        <div id="search"><!-- Campo de pesquisa -->
            <form action="#" method="post">
                <fieldset>
                    <legend class="titulo">Filtrar Resultados . . .</legend>
                    <label class="data">De</label>
                    <input type="text" name="data" maxlength="10" class="input" onkeyup="mascaraData(this);" />
                    &nbsp;<select name="fornecedor">
                        <option>Selecione um Fornecedor...</option>
                        <?php
                        while ( $query2 = mysqli_fetch_array($sql2))
                        {
                            echo "<option value='".$query2['CODIGO']."'>".$query2['NOME']."</option>";
                        }
                        ?>
                    </select><br />
                    <label class="data">At&eacute;</label>
                    <input type="text" name="data2" maxlength="10" class="input" onkeyup="mascaraData2(this);" /><br />
                    <input name="datas" type="submit" class="botao" value="Filtrar"/>
                </fieldset>
            </form>
        </div>
        <?php
        if($_POST['datas'])//filtra pelas datas
        {
            ?>
            <table class="table table-striped table-bordered table-hover" align="center">
                <tbody>
                <tr>
                    <td class='td' style="font-weight:bold;">Cód</td>
                    <td class='td' style="font-weight:bold;">Data</td>
                    <td class='td' style="font-weight:bold;">Fornecedor</td>
                    <td class='td' style="font-weight:bold;">Nota Fiscal</td>
                    <td class='td' style="font-weight:bold;">Valor Nota Fiscal</td>
                </tr>
                <?php
                $codfor = $_POST['fornecedor'];
                $fornecedor = mysqli_query($conn,"SELECT for_nomerazao, cd_fornecedor FROM cad_fornecedores WHERE cd_fornecedor = '$codfor'");
                $data = explode("/",$_POST['data']);
                $dt= $data[2] ."-". $data[1] ."-". $data[0];
                $ate = explode("/",$_POST['data2']);
                $dt2= $ate[2] ."-". $ate[1] ."-". $ate[0];
                if($codfor != 0 && $dt <> '' && $dt2 <> '')
                    $sqlentra = mysqli_query ($conn,"SELECT *, date_format(ent_data, '%d/%m/%Y') AS data FROM entrada_produto WHERE DATA >= '$dt' AND DATA <= '$dt2' AND FORNECEDOR = '$codfor'")or die("ERRO");
                if($dt == 0 && $dt2 == 0)
                    $sqlentra = mysqli_query ($conn,"SELECT *, date_format(ent_data, '%d/%m/%Y') AS data FROM entrada_produto WHERE ent_fornecedor = '$codfor'")or die("ERRO");
                if ($codfor == 0)
                    $sqlentra = mysqli_query ($conn,"SELECT *, date_format(ent_data, '%d/%m/%Y') AS data FROM entrada_produto WHERE ent_data >= '$dt' AND ent_data <= '$dt2'")or die("ERRO");

                while( $result = mysqli_fetch_array($sqlentra))
                {
                    $codigoforn = $result['ent_fornecedor'];
                    $sqlforn = mysqli_query($conn,"SELECT for_nomerazao, cd_fornecedor FROM cad_fornecedores WHERE cd_fornecedor = '$codigoforn'")or die(mysqli_error($conn));
                    $achafor = mysqli_fetch_array($sqlforn);
                    $valornf = number_format($result['ent_valornf'], 2, ',', '.');
                    echo "<tr>";
                    echo "<td class='td'>".$result['cd_entrada']."</td>";
                    echo "<td class='td'>".$result['ent_data']."</td>";
                    echo "<td class='td'>".$achafor['for_nomerazao']."</td>";
                    echo "<td class='td'>".$result['ent_nf']."</td>";
                    echo "<td class='preco'>R$ ".$valornf."</td>";
                    ?>		<td class="nao_imprimir2"><a href="fecha_entrada_produto.php?id=<?php echo $result['cd_entrada']; ?>">Ver</a></td>
                    <?php
                    echo "</tr>";
                }
                ?>			</tbody>
            </table>
            <?php
        }

        else //mostra os resultados normais, sql na linha 54.
        {
        ?>
        <table class="table table-striped table-bordered table-hover" align="center">
            <tbody>
            <tr>
                <td class='td' style="font-weight:bold;">Cód</td>
                <td class='td' style="font-weight:bold;">Data</td>
                <td class='td' style="font-weight:bold;">Fornecedor</td>
                <td class='td' style="font-weight:bold;">Nota Fiscal</td>
                <td class='td' style="font-weight:bold;">Valor Nota Fiscal</td>
            </tr>
            <?php
            $sqlentra = mysqli_query($conn, "SELECT *, date_format(ent_data, '%d/%m/%Y') AS data FROM entrada_produto ORDER BY cd_entrada desc LIMIT 200")or die(mysqli_error($conn));
            while($result = mysqli_fetch_array($sqlentra))
            {
                $codigoforn = $result['ent_fornecedor'];
                $sqlforn = mysqli_query($conn, "SELECT for_nomerazao, cd_fornecedor FROM cad_fonecedores WHERE cd_fornecedor = '$codigoforn'")or die(mysqli_error($conn));
                $achafor = mysqli_fetch_array($sqlforn);
                $valornf = number_format($result['ent_valornf'], 2, ',', '.');

                echo "<tr>";
                echo "<td class='td'>".$result['cd_entrada']."</td>";
                echo "<td class='td'>".$result['ent_data']."</td>";
                echo "<td class='td'>".$achafor['for_nomerazao']."</td>";
                echo "<td class='td'>".$result['ent_nf']."</td>";
                echo "<td class='preco'>R$ ".$valornf." </td>";
                ?>
                <td class="nao_imprimir2"><a href="fecha_entrada_produto.php?id=<?php echo $result['cd_entrada']; ?>">Ver</a></td>
                <?php
                echo "</tr>";
            }	//fim do while

            echo "</tbody>";
            echo "</table>";
            }//fim do else, linha 198
            }//fim do else p exibir os links, linha 125
            ?>

</div>

<script type="text/javascript">
    $('#dataentrada').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>

<script type="text/javascript">
    $(function() {

        //autocomplete
        $("#produto").autocomplete({
            source: "consulta.php",
            minLength: 3
        });

    });
</script>


</body>



</html>
