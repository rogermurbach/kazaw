<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Condição de Pagamento</legend>

    <?php

    $sql2 = mysqli_query($conn, "SELECT * FROM entrada_produto ORDER BY cd_entrada DESC LIMIT 1");
    $result2 = mysqli_fetch_array($sql2);
    $notaf = $result2['ent_nf'];

    $controle = $_POST['controle'];
    //$controle = '5';

    $sql5 = mysqli_query($conn, "select cd_condpag from contas_pagar where conpag_controle = '$controle' limit 1");
    $info = mysqli_fetch_object($sql5);



    // $sql7 = mysqli_query($conn, "delete from contas_pagar where conpag_controle = $controle");




    ?>

        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

        <input type="hidden" name="controle" value="<?php echo $controle; ?>">
        <input type="hidden" name="nritem" value="<?php echo $i; ?>">

        <div class="row">
            <label class="col-md-1 control-label" for="selectbasic">Pagto</label>
            <div class="col-md-3">
                <select id="selectpag" name="formapagamento" class="form-control">
                    <?php

                    $sql= mysqli_query($conn,"SELECT cd_pagamento, con_descricao, con_numpag FROM cond_pagamento order by cd_pagamento asc");
                    while ($result = mysqli_fetch_array($sql) )
                    {

                        ?>
                        <option <?php if($result['cd_pagamento']==$info->cd_condpag){echo " selected=selected";} ?>  value="<?php echo $result['cd_pagamento']; ?>"> <?php echo $result['con_descricao']; ?></option>";

                        <?php
                    }
                    ?>
                </select>
            </div>
            <div class="form-row">
                <div class="col-md-4""><input class="btn btn-primary form-btn" type="submit" name="aplicar" value="Aplicar"></div>
            </div>
        </div>

        <div id="tabelapag">
            <?php

            $sql4 = mysqli_query($conn, "SELECT con_numpag from cond_pagamento where cd_pagamento = $_POST[formapagamento]");
            $resultado = mysqli_fetch_array($sql4);
            $numpag = $resultado['con_numpag'];


            if($_POST['aplicar']) {

            //echo $_POST['formapagamento'];
                $qry = mysqli_query($conn, "delete from contas_pagar where conpag_controle = $controle");


                //$i = 0;
                $data = $result2['ent_data'];
                $valor = $result2['ent_valornf'] / $numpag;

                $sql3 = mysqli_query($conn, "select * from cond_pagamento where cd_pagamento = $_POST[formapagamento]");
                $res = mysqli_fetch_array($sql3);


                for($x=1; $x <= $numpag; $x++){
                    $parc[$x] = $res["con_pag$x"];

                        $qry2 = mysqli_query($conn, "SELECT ADDDATE('$data', INTERVAL $parc[$x] DAY) AS datavenc");
                        $r = (mysqli_fetch_array($qry2));
                        $datavencimento = $r['datavenc'];
                        $qry3 = mysqli_query($conn, "insert into contas_pagar (conpag_controle, conpag_parcela) values ('$controle', '$x')");
                        $qry4 = mysqli_query($conn, "update contas_pagar set cd_condpag = '$_POST[formapagamento]', conpag_duplicata = '$result2[ent_nf]/$x', conpag_nf = '$result2[ent_nf]', conpag_datavecto = '$datavencimento', conpag_valor = '$valor' where conpag_controle = '$controle' and conpag_parcela = '$x'");

                }

                $qry5 = mysqli_query($conn, "select * from contas_pagar where conpag_controle = '$controle'");

                if (($sql) AND ($sql->num_rows != 0)) {
                    ?>
                    <hr>
                    <table class="table table-striped table-bordered table-hover">
                        <legend></legend>

                        <BR>
                        <thead>
                        <tr>
                            <th>Duplicata</th>
                            <th>Nota Fiscal</th>
                            <th>Data de Vencimento</th>
                            <th>Valor (R$)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        while($row_pag = mysqli_fetch_assoc($qry5)){
                            ?>
                            <tr>
                                <th><input id="textinput" name="duplicata[]" type="text" readonly class="form-control input-md col-md-10" value="<?php echo $row_pag['conpag_duplicata']; ?>" ></th>
                                <td><input id="textinput" name="notafiscal[]" type="text" disabled class="form-control input-md col-md-10" value="<?php echo $row_pag['conpag_nf']; ?>"</td>
                                <td><input id="" name="vencimento[]" type="date" class="form-control input-md col-md-10" value="<?php echo $row_pag['conpag_datavecto']; ?>" </td>
                                <td><input id="textinput" name="valor[]" type="text" class="form-control input-md col-md-10" value="<?php echo $row_pag['conpag_valor']; ?>"</td>

                            </tr>
                            <?php

                        }?>
                        </tbody>

                    </table>
                    <?php

                    //$duplicata = $_POST['duplicata'];
                    //$notafiscal = $_POST['notafiscal'];
                    $vencimento = $_POST['vencimento'];
                    $valordp = $_POST['valor'];
                    $ii = 1;

                    for($i=0; $i<=$numpag; $i++){

                        $qr6 = mysqli_query($conn, "UPDATE contas_pagar set conpag_datavecto = '$vencimento[$i]', conpag_valor = '$valordp[$i]' where conpag_controle = '$controle' and conpag_parcela = '$ii'");
                        $ii++;
                    }

                    ?>



                    <BR>
                    <?php
                } else {
                    echo "<div class='alert alert-danger' role='alert'>Nenhum registro encontrado!</div><BR>";

                }
            }
    ?>


        </div>




</fieldset>
</form>

<form action="fecha_entrada_produto.php?id=<?php echo $controle; ?>" method="post">
    <div class="form-row">
        <div class="col-md-12 content-right" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" type="submit" name="gravar" value="Finalizar">  </div>
    </div>
</form>

</div>

<script type="text/javascript">
    $('#dataentrada').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>




</body>



</html>
