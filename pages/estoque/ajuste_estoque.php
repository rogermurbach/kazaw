<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="novo_ajuste_estoque.php" method="get">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Ajuste de Estoque</legend>

    <?php

    $sql= mysqli_query($conn,"SELECT * FROM ajuste_estoque");
    $sql2 = mysqli_query($conn,"SELECT AUTO_INCREMENT AS cd_ajuste FROM information_schema.tables WHERE table_name = 'ajuste_estoque' AND table_schema = 'painel2'");
    $info = mysqli_fetch_array($sql2);
    $info2 = mysqli_fetch_array($sql);

    $controle = $info['cd_ajuste'];

    //echo $controle;

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

        <input type="hidden" name="controle" value="<?php echo $controle; ?>">


        <div class="row" >
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_ajuste" type="text" readonly value="<?php echo $controle; ?>" class="form-control input-md">
        </div>
  </div>

        <div class="row">
            <label class="col-md-1 control-label" for="selectbasic">Data</label>
            <div class="col-md-2">
                <div class="input-group date">
                    <input type="text" class="form-control" id="dataentrada" name="dataajuste" >
                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Motivo</label>
            <div class="col-md-7">
                <input id="textinput" name="motivo" type="text" value="" class="form-control input-md">
            </div>
        </div>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="enviar" type="submit" value="Incluir" /> <button class="btn btn-danger form-btn" type="reset" href="">Limpar </button></div>
                      </div>
        <hr>
</fieldset>
</form>


</div>

<script type="text/javascript">
    $('#dataentrada').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>


</body>



</html>
