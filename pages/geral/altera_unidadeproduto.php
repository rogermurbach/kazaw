<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

    <script>
        function callconfirm(id)
        {
            bootbox.confirm({
                message: "TEM CERTEZA QUE DESEJA EXCLUIR ESTE CADASTRO?",
                buttons: {
                    confirm: {
                        label: 'SIM',
                        className: 'btn-success',
                        callback: function(){
                            window.location.href = "deleta_unidade.php"
                        }
                    },
                    cancel: {
                        label: 'NÃO',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    // console.log('This was logged in the callback: ' + result);
                    if(result == true){
                        window.location.href = "deleta_unidade.php?id="+id
                    }else{

                    }

                }
            });

        }
    </script>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="#" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Cadastro de Unidade de Produtos</legend>

    <?php

    $qry = "select * from unidade_produto where cd_unidade=$_GET[id]";
    $sql = mysqli_query($conn, $qry);
    $info = mysqli_fetch_object($sql);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_unidade" type="text" readonly value="<?php echo $info->cd_unidade ?>" class="form-control input-md">
        </div>
  </div>
        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Abreviação</label>
            <div class="col-md-1">
                <input id="textinput" name="abreviado" type="text" value="<?php echo $info->uni_sigla ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
            </div>
        </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Descrição</label>
            <div class="col-md-5">
                <input id="textinput" name="descricao" type="text" value="<?php echo $info->uni_descricao ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
            </div>
        </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


        <div class="form-row">
            <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" type="submit" name="gravar" value="Salvar"> </input> <button class="btn btn-secondary form-btn" type="button" onclick="callconfirm('<?php echo $info->cd_unidade ?>')" >Excluir </button> <input class="btn btn-danger form-btn" type="button" onclick="form.action='unidade_produtos.php'; form.submit()" value="Cancelar" >  </div>
        </div>

</fieldset>
</form>

    <?php

    include_once ("../../includes/config.php");

    $codigo = $_POST["cd_unidade"];
    $sigla = $_POST["abreviado"];
    $descricao = $_POST["descricao"];

    if($_POST['gravar']){

        $sql = mysqli_query($conn, "update unidade_produto set uni_sigla = upper('$sigla'), uni_descricao = upper('$descricao') where cd_unidade = $codigo");

        if ($sql) { // verificação para saber se foi cadastrado
            $_SESSION['msgcad'] = "<BR><div class='alert alert-success'>Unidade de produto alterada com sucesso!</div>";
            header("Location: altera_unidadeproduto.php?id=$codigo");


        } else { // Caso dê erro
            $_SESSION['msg'] = "<BR><div class='alert alert-danger'>Erro ao alterar a unidade de produto!</div>"; mysqli_error($sql);
            //$_SESSION["msgcad"] ="Não foi possível incluir " .mysqli_error($sql);
            header("Location: altera_unidadeproduto.php?id=$codigo");

        }


    }

    ?>


</div>

</body>



</html>
