<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">
<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

</head>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">
<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">
  <form class="form-horizontal" action="#" method="post">
      <?php
      if (isset($_SESSION['msg'])){
          echo $_SESSION['msg'];
          unset($_SESSION['msg']);
      }
      if (isset($_SESSION['msgcad'])){
          echo $_SESSION['msgcad'];
          unset($_SESSION['msgcad']);
      }
      ?>

<fieldset>

<!-- Form Name -->
<legend>Histórico de Ações</legend>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:15px">
          </div>
        </div>
      </li>

      <div class="row">
          <label class="col-md-1 control-label" for="selectbasic">Rotina</label>
          <div class="col-md-5">
              <select id="selectbasic" name="filtro_tipo" class="form-control">
                  <option value="all">- TODAS -</option>
                  <option value="CADCLI">Cadastro de Cliente</option>
                  <option value="ALTCLI">Alteração de Cliente</option>
                  <option value="CADPROD">Cadastro de Produto</option>
                  <option value="ALTPROD">Alteração de Produto</option>
                  <option value="ENTPED">Entrada de Produto</option>
              </select>
      </div>
        </div>
        <div class="row">
          <label class="col-md-1 control-label" for="selectbasic">Data</label>
            <div class="col-md-5">
                <div class="input-group date">
                    <label class="col-md-2 control-label" for="selectbasic">De</label>
                    <input type="text" class="form-control col-md-10" id="datalog1" name="datalog1" >
                    <label class="col-md-2 control-label" for="selectbasic">Até</label>
                    <input type="text" class="form-control col-md-10" id="datalog2" name="datalog2" >

                    <div class="input-group-addon">
                        <span class="glyphicon glyphicon-th"></span>
                    </div>
                </div>
            </div>
        </div>

    <div class="form-row">
            <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" type="submit" name="filtrar" value="Aplicar"> </input><button class="btn btn-danger form-btn" type="reset">Limpar </button></div>
        </div>


</fieldset>
</form>

    <?php


    $datalog1 = $_POST['datalog1'];
    $datalog2 = $_POST['datalog2'];
    $datalog1 = date("Y-m-d",strtotime(str_replace('/','-',$datalog1)));
    $datalog2 = date("Y-m-d",strtotime(str_replace('/','-',$datalog2)));

    if($_POST['filtrar']){

            //include "includes/config.php";


            if($_POST['filtro_tipo'] == "all"){
                $sql = "select * from log_geral where cast(log_data as date) BETWEEN '$datalog1' AND '$datalog2'";
                $res = mysqli_query($conn, $sql);
            }elseif($_POST['filtro_tipo'] == "CADPROD"){
                $sql = "select * from log_geral where log_rotina = '$_POST[filtro_tipo]' and cast(log_data as date) BETWEEN '$datalog1' AND '$datalog2'";
                $res = mysqli_query($conn, $sql);
            }elseif($_POST['filtro_tipo'] == "ALTPROD") {
                $sql = "select * from log_geral where log_rotina = '$_POST[filtro_tipo]' and cast(log_data as date) BETWEEN '$datalog1' AND '$datalog2'";
                $res = mysqli_query($conn, $sql);
            }elseif ($_POST['filtro_tipo'] == "ENTPED"){
                $sql = "select * from log_geral where log_rotina = '$_POST[filtro_tipo]' and cast(log_data as date) BETWEEN '$datalog1' AND '$datalog2'";
                $res = mysqli_query($conn, $sql);
            }elseif ($_POST['filtro_tipo'] == "CADCLI"){
                $sql = "select * from log_geral where log_rotina = '$_POST[filtro_tipo]' and cast(log_data as date) BETWEEN '$datalog1' AND '$datalog2'";
                $res = mysqli_query($conn, $sql);
            }elseif ($_POST['filtro_tipo'] == "ALTCLI"){
                $sql = "select * from log_geral where log_rotina = '$_POST[filtro_tipo]' and cast(log_data as date) BETWEEN '$datalog1' AND '$datalog2'";
                $res = mysqli_query($conn, $sql);
            }


            //consultar no banco de dados
            //$sql = "SELECT * FROM cad_clientes WHERE cli_nomerazao like '%$_GET[filtro_nome]%' and cd_cliente = '$_GET[filtro_nome]' and tp_cliente = '$_GET[filtro_tipo]' ORDER BY cd_cliente ASC";
            //$res = mysqli_query($conn, $sql);
            //$sql = "SELECT * FROM cad_clientes WHERE tp_cliente = 'f' ORDER BY cd_cliente ASC";


            //Verificar se encontrou resultado na tabela "usuarios"
            if(($res) AND ($res->num_rows != 0)){
                ?>
                <hr>
                <table class="table table-striped table-bordered table-hover">
                    <legend>Resultado da busca:</legend>
                    <BR>
                    <thead>
                    <tr>
                        <th width="40">Cód</th>
                        <th width="200">Data</th>
                        <th>Rotina</th>
                        <th width="100">Cód da Rotina</th>
                        <th>Descrição</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row_cli = mysqli_fetch_assoc($res)){
                        $id = $row_cli['cd_log'];
                        $_SESSION['id'] = $id;
                        ?>
                        <tr>
                            <th><?php echo $row_cli['cd_log']; ?></th>
                            <td><?php echo $row_cli['log_data']; ?></td>
                            <td><?php echo $row_cli['log_rotina']; ?></td>
                            <td><?php echo $row_cli['log_codrotina']; ?></td>
                            <td><?php echo $row_cli['log_descricao']; ?></td>
                        </tr>
                        <?php
                    }?>
                    </tbody>

                </table>
                <BR>
                <?php
            }else{
                echo "<div class='alert alert-danger' role='alert'>Nenhum registro encontrado!</div><BR>";

            }
        }
    ?>


</div>

<script type="text/javascript">
    $('#datalog1').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true,
        multidate: true
    }).datepicker("setDate", "0");
</script>
<script type="text/javascript">
    $('#datalog2').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR",
        todayHighlight: true
    }).datepicker("setDate", "0");
</script>

</body>



</html>
