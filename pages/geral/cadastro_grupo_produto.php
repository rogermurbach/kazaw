<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="#" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Cadastro de Grupo de Produtos</legend>

    <?php

    $sql= mysqli_query($conn,"SELECT cd_grupo FROM grupo_produtos order by cd_grupo desc");
    $info = mysqli_fetch_array($sql);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_grupo" type="text" readonly value="<?php $info = $info['cd_grupo'] + 1; echo $info ?>" class="form-control input-md">
        </div>
  </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Descrição</label>
            <div class="col-md-7">
                <input id="textinput" name="descricao" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
            </div>
        </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <input class="btn btn-danger form-btn" type="button" onclick="form.action='grupo_produtos.php'; form.submit()" value="Cancelar" ></div>
                      </div>

</fieldset>
</form>

    <?php

    include_once ("../../includes/config.php");

    $codigo = $_POST["cd_grupo"];
    $descricao = $_POST["descricao"];

    $rotina = "CADGRP";
    $codrotina = $codigo;
    $descricaolog = "Cadastro de grupo de produto - $descricao";

    if($_POST['gravar']){

        if (!mysqli_query($conn, "INSERT INTO grupo_produtos (gru_produto) VALUES (UPPER('$descricao'))"))
          {
            echo("Erro: " . mysqli_error($conn));
            echo '<script type="text/javascript">toastr.error("Erro ao cadastrar grupo de produto!")</script>';
          }else{
            echo '<script type="text/javascript">toastr.success("Grupo cadastrado com sucesso!")</script>';
            $sql1 = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
            header("Location: grupo_produtos.php");
        }

    }

    ?>


</div>
<script type="text/javascript">
    $('#datacadastro').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    });
</script>

</body>



</html>
