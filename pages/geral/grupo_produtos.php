<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">
<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

</head>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">
<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">
  <form class="form-horizontal" action="#" method="post">
      <?php
      if (isset($_SESSION['msg'])){
          echo $_SESSION['msg'];
          unset($_SESSION['msg']);
      }
      if (isset($_SESSION['msgcad'])){
          echo $_SESSION['msgcad'];
          unset($_SESSION['msgcad']);
      }
      ?>

<fieldset>

<!-- Form Name -->
<legend>Grupo de Produtos</legend>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:15px">
          </div>
        </div>
      </li>

      <div class="form-row">
          <div class="col-md-12 content-center"><a href="cadastro_grupo_produto.php"><button class="btn btn-primary form-btn" type="button">Incluir </button></a><button class="btn btn-danger form-btn" type="reset">Exportar </button></div>
      </div>


    <?php

    $sql = "SELECT * FROM grupo_produtos ORDER BY gru_produto ASC";
    $res = mysqli_query($conn, $sql);

    /*  if($_POST['filtrar']){

          //include "includes/config.php";


          if($_POST['filtro_tipo'] == "all"){
              $sql = "SELECT * FROM produtos WHERE pro_descricao like '%$_GET[filtro_nome]%' or cd_produto = '$_GET[filtro_nome]' ORDER BY cd_produto ASC";
              $res = mysqli_query($conn, $sql);
          }elseif ($_POST['filtro_tipo'] == "f") {
              $sql = "SELECT * FROM produtos WHERE pro_descricao like '%$_GET[filtro_nome]%' or cd_produto = '$_GET[filtro_nome]' and tp_cliente = 'f' ORDER BY cd_cliente ASC";
              $res = mysqli_query($conn, $sql);
          }else{
              $sql = "SELECT * FROM produtos WHERE prod_descricao like '%$_GET[filtro_nome]%' or cd_produto = '$_GET[filtro_nome]' and tp_cliente = 'j' ORDER BY cd_cliente ASC";
              $res = mysqli_query($conn, $sql);
          }*/


    //consultar no banco de dados
    //$sql = "SELECT * FROM cad_clientes WHERE cli_nomerazao like '%$_GET[filtro_nome]%' and cd_cliente = '$_GET[filtro_nome]' and tp_cliente = '$_GET[filtro_tipo]' ORDER BY cd_cliente ASC";
    //$res = mysqli_query($conn, $sql);
    //$sql = "SELECT * FROM cad_clientes WHERE tp_cliente = 'f' ORDER BY cd_cliente ASC";


    //Verificar se encontrou resultado na tabela "usuarios"
    if(($res) AND ($res->num_rows != 0)){
        ?>
        <hr>
        <table class="table table-striped table-bordered table-hover">
            <legend>Grupos Cadastrados:</legend>
            <BR>
            <thead>
            <tr>
                <th width="40">Código</th>
                <th width="300">Descrição</th>
            </tr>
            </thead>
            <tbody>
            <?php
            while($row_cli = mysqli_fetch_assoc($res)){
                $id = $row_cli['cd_grupo'];
                $_SESSION['id'] = $id;
                ?>
                <tr>
                    <th><?php echo $row_cli['cd_grupo']; ?></th>
                    <td><a href="altera_grupoproduto.php?id=<?php echo $row_cli['cd_grupo'] ?>"><?php echo $row_cli['gru_produto']; ?></a></td>
                </tr>
                <?php
            }?>
            </tbody>

        </table>
        <BR>
        <?php
    }else{
        echo "<div class='alert alert-danger' role='alert'>Nenhum produto encontrado!</div><BR>";


    }
    ?>

     <!-- <div class="row">
        <label class="col-md-1 control-label" for="textinput">Filtro</label>
        <div class="col-md-5">
        <input id="textinput" name="filtro_nome" type="text" placeholder="Digite o código ou parte do nome" class="form-control input-md">
      </div>
        </div>
        <div class="row">
          <label class="col-md-1 control-label" for="selectbasic">Grupo</label>
          <div class="col-md-5">
            <select id="selectbasic" name="filtro_tipo" class="form-control">
              <option value="all">- Todos -</option>
            </select>
          </div>
        </div>
        <div class="form-row">
            <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" type="submit" name="filtrar" value="Aplicar"> </input><button class="btn btn-danger form-btn" type="reset">Limpar </button></div>
        </div>-->




</fieldset>
</form>




</div>

</body>



</html>
