<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="#" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Cadastro de Unidade de Produtos</legend>

    <?php

    $sql= mysqli_query($conn,"SELECT cd_unidade FROM unidade_produto order by cd_unidade desc");
    $info = mysqli_fetch_array($sql);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_unidade" type="text" readonly value="<?php $info = $info['cd_unidade'] + 1; echo $info ?>" class="form-control input-md">
        </div>
  </div>
        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Abreviação</label>
            <div class="col-md-1">
                <input id="textinput" name="abreviado" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
            </div>
        </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Descrição</label>
            <div class="col-md-5">
                <input id="textinput" name="descricao" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
            </div>
        </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <input class="btn btn-danger form-btn" type="button" onclick="form.action='unidade_produtos.php'; form.submit()" value="Cancelar" ></div>
                      </div>

</fieldset>
</form>

    <?php

    include_once ("../../includes/config.php");

    $codigo = $_POST["cd_unidade"];
    $sigla = $_POST["abreviado"];
    $descricao = $_POST["descricao"];

    if($_POST['gravar']){

        $sql = mysqli_query($conn, "INSERT INTO unidade_produto (uni_sigla, uni_descricao) VALUES (UPPER('$sigla'), UPPER('$descricao'))");

        if ($sql) { // verificação para saber se foi cadastrado
            $_SESSION['msgcad'] = "<BR><div class='alert alert-success'>Unidade de produto cadastrado com sucesso!</div>";
            header("Location: unidade_produtos.php");


        } else { // Caso dê erro
            $_SESSION['msg'] = "<BR><div class='alert alert-danger'>Erro ao cadastrar a unidade de produto!</div>"; mysqli_error($sql);
            //$_SESSION["msgcad"] ="Não foi possível incluir " .mysqli_error($sql);
            header("Location: cadastro_unidade_produto.php");

        }


    }

    ?>


</div>
<script type="text/javascript">
    $('#datacadastro').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    });
</script>

</body>



</html>
