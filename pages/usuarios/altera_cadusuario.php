<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache" >

<head>
    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

    <script>
        function callconfirm(id)
        {
            bootbox.confirm({
                message: "TEM CERTEZA QUE DESEJA EXCLUIR ESTE CADASTRO?",
                buttons: {
                    confirm: {
                        label: 'SIM',
                        className: 'btn-success',
                        callback: function(){
                            window.location.href = "deleta_usuario.php"
                        }
                    },
                    cancel: {
                        label: 'NÃO',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                   // console.log('This was logged in the callback: ' + result);
                    if(result == true){
                        window.location.href = "deleta_usuario.php?id="+id
                    }else{

                    }

                }
            });

        }
    </script>


</head>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">
<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">
  <form class="form-horizontal" action="" method="POST">
    <?php
      if (isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
      }
      if (isset($_SESSION['msgcad'])){
        echo $_SESSION['msgcad'];
        unset($_SESSION['msgcad']);
      }
    ?>
<fieldset>

<!-- Form Name -->

<legend>Alterar Cadastro de Cliente</legend>

<?php
//include ("includes/config.php");

$qry = "select * from usuarios where usuario_id=$_GET[id]";
$sql = mysqli_query($conn, $qry);
$info = mysqli_fetch_object($sql);

$sql2 =  mysqli_query($conn, "SELECT * FROM estados order by sigla asc");


?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cod_user" type="text" value="<?php echo $info->usuario_id; ?>" readonly="true" class="form-control input-md">
        </div>

  </div>


<!-- Text input-->
<div class="row">
  <label class="col-md-1 control-label" for="textinput">Nome *</label>
  <div class="col-md-5">
    <input id="textinput" name="nome" type="text" value="<?php echo $info->nome; ?>" placeholder="" autocomplete="off" required="" class="form-control input-md">
  </div>
  <label class="col-md-1 control-label" for="textinput">Login</label>
    <div class="col-md-2">
      <input  name="login" id="login" value="<?php echo $info->usuario; ?>" type="text" class="form-control input-md">
    </div>
</div>

  <!-- Select Basic -->
  <div class="row">
    <label class="col-md-1 control-label" for="selectbasic">Nivel</label>
    <div class="col-md-5">
      <select id="selectbasic" name="nivel" required class="form-control">
        <option label="- Selecione o nível -"></option>
        <option value="0" <?php if($info->nivel_usuario == '0') echo 'selected'; ?>>Usuários</option>
        <option value="2" <?php if($info->nivel_usuario == '2') echo 'selected'; ?>>Administrador</option>
      </select>
    </div>

    <label class="col-md-1 control-label" for="textinput">Senha</label>
      <div class="col-md-2">
        <input  name="senha" id="senha" value="" type="password" class="form-control input-md" required>
      </div>

  </div>


  <div class="row">
    <label class="col-md-1 control-label" for="textinput">Fone</label>
    <div class="col-md-5">
      <input id="telefone" name="fone" value="<?php echo $info->fone; ?>" type="text"  maxlength="14"  placeholder="DDD + Telefone" autocomplete="off" class="form-control input-md">
    </div>

  </div>

    <div class="row">
      <label class="col-md-1 control-label" for="textinput">Data de Nascimento</label>
      <div class="col-md-3">
          <input type="text" class="form-control" id="datanascimento" name="nascimento" value="<?php echo date('d/m/Y',$info->data_nasc); ?>" >
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <a>Digite o CEP para preencher o endereço</a>
      </div>
    </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Endereço</label>
        <div class="col-md-6">
        <input name="rua" id="rua" type="text" value="<?php echo $info->endereco; ?>" class="form-control input-md">
      </div>
      <label class="col-md-1 control-label" for="textinput">Numero</label>
        <div class="col-md-3">
        <input name="numero" type="text" value="<?php echo $info->numero; ?>" class="form-control input-md">
        </div>
        </div>

        <div class="row">
          <label class="col-md-1 control-label" for="textinput">Bairro</label>
          <div class="col-md-5">
          <input name="bairro" id="bairro" value="<?php echo $info->bairro; ?>" type="text" placeholder="" autocomplete="off" class="form-control input-md">
        </div>
        <label class="col-md-1 control-label" for="textinput">Complem.</label>
          <div class="col-md-4">
          <input name="complemento" id="complemento" value="<?php echo $info->complemento; ?>" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          </div>

          <div class="row">
            <label class="col-md-1 control-label" for="textinput">Cidade</label>
            <div class="col-md-5">
            <input name="cidade" id="cidade" value="<?php echo $info->cidade; ?>" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          <label class="col-md-1 control-label" for="selectbasic">Estado</label>
          <div class="col-md-1">
            <select id="estado" name="estado" class="form-control">
                <?php
                $sql2 =  mysqli_query($conn, "SELECT * FROM estados order by sigla asc");
                while ($result = mysqli_fetch_array($sql2) )
                {
                    ?>
                    <option value="<?php echo $result['id_estado'];?>" <?php if($result['id_estado'] == $info->estado){ echo 'selected'; } ?> ><?php echo $result['sigla']; ?></option>";
                <?php 
                }
                ?>
            </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">CEP</label>
            <div class="col-md-2">
              <input id="textinput" name="cep" id="cep" value="<?php echo $info->cep; ?>" type="text" onblur="pesquisacep(this.value);" class="form-control input-md">
            </div>
            </div>

            <div class="row">
              <label class="col-md-1 control-label" for="textinput">Email</label>
              <div class="col-md-5">
              <input id="textinput" name="email" value="<?php echo $info->email; ?>" type="text" placeholder="" autocomplete="off" class="form-control input-md">
            </div>
              </div>

              <div class="row">
                <label class="col-md-1 control-label" for="textinput">CPF</label>
                <div class="col-md-5">
                <input id="textinput" name="cpf" value="<?php echo $info->cpf; ?>" type="text" maxlength="14" placeholder="" autocomplete="off" class="form-control input-md">
              </div>
              <label class="col-md-1 control-label" for="textinput">Identidade</label>
                <div class="col-md-4">
                <input id="textinput" name="identidade" value="<?php echo $info->rg; ?>" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                </div>
                </div>

                <div class="row">
                  <label class="col-md-1 control-label" for="textinput">Observação</label>
                  <div class="col-md-10">
                    <input id="textinput" name="obs" value="<?php echo $info->info; ?>" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                  </div>
                </div>

                <div class="row">
                  <label class="col-md-1 control-label" for="selectbasic">Status do login</label>
                    <div class="col-md-5">
                      <select id="selectbasic" name="status" required class="form-control">
                        <option label="- Selecione a opção -"></option>
                        <option value="1" <?php if($info->ativado == '1') echo 'selected'; ?>>Ativado</option>
                        <option value="0" <?php if($info->ativado == '0') echo 'selected'; ?>>Desativado</option>
                      </select>
                    </div>
                </div>


                <div id="cid_6" class="form-input-wide">
                  <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"></div>
                </div>
              </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <button class="btn btn-secondary form-btn" type="button" onclick="callconfirm('<?php echo $info->usuario_id ?>')" >Excluir  <button class="btn btn-danger form-btn" type="reset" href="#" onclick="form.action='consulta_usuario.php'; form.submit()">Cancelar </button></div>
                      </div>

</fieldset>
</form>

    <?php

    //include_once ("includes/config.php");

    if($_POST['gravar']){

      $codigo = $_POST["cod_user"];
      $nome = $_POST["nome"];
      $fone = $_POST["fone"];
      $nascimento = $_POST["nascimento"];
      $rua = $_POST["rua"];
      $numero = $_POST["numero"];
      $bairro = $_POST["bairro"];
      $complemento = $_POST["complemento"];
      $cidade = addslashes($_POST["cidade"]);
      $estado = $_POST["estado"];
      $cep = $_POST["cep"];
      $email = $_POST["email"];
      $cpf = $_POST["cpf"];
      $identidade = $_POST["identidade"];
      $observacao= $_POST["obs"];
      $nivel = $_POST['nivel'];
      $login = $_POST['login'];
      $senha = md5($_POST['senha']);
      $status = $_POST['status'];

      $nascimento = date("Y-m-d");
      $nascimento = strtotime($nascimento);

      $rotina = "ALTUSR";
      $codrotina = $codigo;
      $descricaolog = "Altera usuario - $nome";

        if (!mysqli_query($conn, "UPDATE usuarios SET nome = '" . addslashes($nome) . "', fone = '" . $fone . "' , data_nasc = '" . $nascimento . "', endereco = '" . addslashes($rua) . "',".
         " numero = '" . $numero . "', bairro = '" . addslashes($bairro) . "', complemento = '" . addslashes($complemento) . "', cidade = '" . addslashes($cidade) . "',".
         " estado = '" . addslashes($estado) . "', cep = '" . $cep . "', email = lower('$email'), cpf = '" . $cpf . "', rg = '" . $identidade . "', info = '" . addslashes($observacao) . "',".
         " nivel_usuario = '" . $nivel . "', usuario = '" . $login . "', senha = '" . $senha . "', ativado = '" . $status . "' WHERE usuario_id = $codigo "))
        {
          echo("Erro: " . mysqli_error($conn));
          echo '<script type="text/javascript">toastr.error("Erro ao alterar Usuario!")</script>';
        }else{
          echo '<script type="text/javascript">toastr.success("Usuario alterado com sucesso!")</script>';
          $sql1 = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
          header("Refresh:0");
        }


    }

    ?>


</div>

<script type="text/javascript">
    $('#datanascimento').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    });
</script>



</body>




</html>
