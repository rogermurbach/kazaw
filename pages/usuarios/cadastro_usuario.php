<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <script type="text/javascript" >

        function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('estado').value=("");

        }

        function meu_callback(conteudo) {
            if (!("erro" in conteudo)) {
                //Atualiza os campos com os valores.
                document.getElementById('rua').value=(conteudo.logradouro);
                document.getElementById('bairro').value=(conteudo.bairro);
                document.getElementById('cidade').value=(conteudo.localidade);
                $("#estado option").filter(function() {
		              return this.text == conteudo.uf; 
	              }).attr('selected', true);

            } //end if.
            else {
                //CEP não Encontrado.
                limpa_formulário_cep();
                alert("CEP não encontrado.");
            }
        }

        function pesquisacep(valor) {

            //Nova variável "cep" somente com dígitos.
            var cep = valor.replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    document.getElementById('rua').value="...";
                    document.getElementById('bairro').value="...";
                    document.getElementById('cidade').value="...";
                    document.getElementById('estado').value="...";


                    //Cria um elemento javascript.
                    var script = document.createElement('script');

                    //Sincroniza com o callback.
                    script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                    //Insere script no documento e carrega o conteúdo.
                    document.body.appendChild(script);

                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        };

    </script>



    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>


</head>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
include_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

  <form class="form-horizontal" action="#" method="post">
    <?php
      if (isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
      }
      if (isset($_SESSION['msgcad'])){
        echo $_SESSION['msgcad'];
        unset($_SESSION['msgcad']);
      }
    ?>
<fieldset>
<!-- Form Name -->

<legend>Cadastro de Clientes</legend>

    <?php
    //$database_connect = "teste";

    $qry = " SELECT AUTO_INCREMENT as usuario_id FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $database_connect . "' AND TABLE_NAME = 'usuarios' ";
    $sql_id = mysqli_query($conn, $qry);
    $info = mysqli_fetch_array($sql_id);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cod_user" type="text" readonly value="<?php echo $info['usuario_id'] ?>" class="form-control input-md">
        </div>

  </div>

<!-- Text input-->
<div class="row">
  <label class="col-md-1 control-label" for="textinput">Nome *</label>
  <div class="col-md-5">
    <input id="textinput" name="nome" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
  </div>
  <label class="col-md-1 control-label" for="textinput">Login</label>
    <div class="col-md-2">
      <input  name="login" id="login" value="" type="text" class="form-control input-md">
    </div>
</div>

  <!-- Select Basic -->
  <div class="row">
    <label class="col-md-1 control-label" for="selectbasic">Nivel</label>
    <div class="col-md-5">
      <select id="selectbasic" name="nivel" required class="form-control">
        <option label="- Selecione o nível -"></option>
        <option value="0">Usuários</option>
        <option value="2">Admnistrador</option>
      </select>
    </div>

    <label class="col-md-1 control-label" for="textinput">Senha</label>
      <div class="col-md-2">
        <input  name="senha" id="senha" value="" type="password" class="form-control input-md">
      </div>

  </div>


  <div class="row">
    <label class="col-md-1 control-label" for="textinput">Fone</label>
    <div class="col-md-5">
      <input id="telefone" name="fone" value="" type="text"  maxlength="14"  placeholder="DDD + Telefone" autocomplete="off" class="form-control input-md">
    </div>

  </div>

    <div class="row">
      <label class="col-md-1 control-label" for="textinput">Data de Nascimento</label>
      <div class="col-md-3">
          <input type="text" class="form-control" id="datanascimento" name="nascimento" >
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <a>Digite o CEP para preencher o endereço</a>
      </div>
    </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Endereço</label>
        <div class="col-md-6">
        <input name="rua" id="rua" type="text" class="form-control input-md">
      </div>
      <label class="col-md-1 control-label" for="textinput">Numero</label>
        <div class="col-md-3">
        <input name="numero" type="text" class="form-control input-md">
        </div>
        </div>

        <div class="row">
          <label class="col-md-1 control-label" for="textinput">Bairro</label>
          <div class="col-md-5">
          <input name="bairro" id="bairro" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
        </div>
        <label class="col-md-1 control-label" for="textinput">Complem.</label>
          <div class="col-md-4">
          <input name="complemento" id="complemento" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          </div>

          <div class="row">
            <label class="col-md-1 control-label" for="textinput">Cidade</label>
            <div class="col-md-5">
            <input name="cidade" id="cidade" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          <label class="col-md-1 control-label" for="selectbasic">Estado</label>
          <div class="col-md-1">
            <select id="estado" name="estado" class="form-control">
                <?php
                $sql2 =  mysqli_query($conn, "SELECT * FROM estados order by sigla asc");
                while ($result = mysqli_fetch_array($sql2) )
                {
                    ?>
                    <option value="<?php echo $result['id_estado']; ?>"><?php echo $result['sigla']; ?></option>";
                <?php
                }
                ?>
            </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">CEP</label>
            <div class="col-md-2">
              <input id="textinput" name="cep" id="cep" value="" type="text" onblur="pesquisacep(this.value);" class="form-control input-md">
            </div>
            </div>

            <div class="row">
              <label class="col-md-1 control-label" for="textinput">Email</label>
              <div class="col-md-5">
              <input id="textinput" name="email" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
            </div>
              </div>

              <div class="row">
                <label class="col-md-1 control-label" for="textinput">CPF</label>
                <div class="col-md-5">
                <input id="textinput" name="cpf" value="" type="text" maxlength="14" placeholder="" autocomplete="off" class="form-control input-md">
              </div>
              <label class="col-md-1 control-label" for="textinput">Identidade</label>
                <div class="col-md-4">
                <input id="textinput" name="identidade" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                </div>
                </div>

                <div class="row">
                  <label class="col-md-1 control-label" for="textinput">Observação</label>
                  <div class="col-md-10">
                    <input id="textinput" name="obs" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                  </div>
                </div>

                <div class="row">
                  <label class="col-md-1 control-label" for="selectbasic">Status do login</label>
                    <div class="col-md-5">
                      <select id="selectbasic" name="status" required class="form-control">
                        <option label="- Selecione a opção -"></option>
                        <option value="1">Ativado</option>
                        <option value="0">Desativado</option>
                      </select>
                    </div>
                </div>


                <div id="cid_6" class="form-input-wide">
                  <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"></div>
                </div>
              </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <button class="btn btn-danger form-btn" type="reset" href="#" onclick="window.history.back()">Cancelar </button></div>
                      </div>

</fieldset>
</form>

    <?php

    include_once ("../../includes/config.php");

    if($_POST['gravar']){

        $codigo = $_POST["cod_user"];
        $nome = $_POST["nome"];
        $fone = $_POST["fone"];
        $nascimento = $_POST["nascimento"];
        $rua = $_POST["rua"];
        $numero = $_POST["numero"];
        $bairro = $_POST["bairro"];
        $complemento = $_POST["complemento"];
        $cidade = addslashes($_POST["cidade"]);
        $estado = $_POST["estado"];
        $cep = $_POST["cep"];
        $email = $_POST["email"];
        $cpf = $_POST["cpf"];
        $identidade = $_POST["identidade"];
        $observacao= $_POST["obs"];
        $nivel = $_POST['nivel'];
        $login = $_POST['login'];
        $senha = md5($_POST['senha']);
        $status = $_POST['status'];

        $nascimento = date("Y-m-d");
        $nascimento = strtotime($nascimento);

        $rotina = "CADUSR";
        $codrotina = $codigo;
        $descricaolog = "Cadastro de usuario - $nome";

        if (!mysqli_query($conn, "INSERT INTO usuarios (nome, fone, data_nasc, endereco, numero, bairro, complemento, cidade, estado, cep, email, cpf, rg, info, nivel_usuario, usuario, senha, ativado)".
                 " VALUES('" . addslashes($nome) . "','" . $fone . "','" . $nascimento . "','" . addslashes($rua) . "','" . $numero . "','" . addslashes($bairro) . "','" . addslashes($complemento) . "',".
                 " '" . addslashes($cidade) . "','" . addslashes($estado) . "','" . $cep . "',lower('$email'),'" . $cpf . "','" . $identidade . "','" . addslashes($observacao) . "','" . $nivel . "','" . $login . "','" . $senha . "','" . $status . "')"))
          {
            echo("Erro: " . mysqli_error($conn));
            echo '<script type="text/javascript">toastr.error("Erro ao cadastrar Usuario!")</script>';
          }else{
            echo '<script type="text/javascript">toastr.success("Usuario cadastrado com sucesso!")</script>';
            $sql1 = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
            header("Location: consulta_usuario.php");
        }
    }

    ?>
</div>

<script type="text/javascript">
    $('#datanascimento').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    });
</script>



</body>



</html>
