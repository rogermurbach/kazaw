<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">
<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

</head>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">
<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');

?>

<div class="ctd" id="conteudo">
  <form class="form-horizontal" action="#" method="post">
      <?php
      if (isset($_SESSION['msg'])){
          echo $_SESSION['msg'];
          unset($_SESSION['msg']);
      }
      if (isset($_SESSION['msgcad'])){
          echo $_SESSION['msgcad'];
          unset($_SESSION['msgcad']);
      }
      ?>

<fieldset>

<!-- Form Name -->
<legend>Consulta de Usuários</legend>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:5px;margin-bottom:15px">
          </div>
        </div>
      </li>

      <div class="form-row">
          <div class="col-md-12 content-center"><a href="cadastro_usuario.php"><button class="btn btn-primary form-btn" type="button">Incluir </button></a><button class="btn btn-danger form-btn" type="reset">Exportar </button></div>
      </div>
      <hr>


    <div class="row">
        <label class="col-md-1 control-label" for="selectbasic">Filtro</label>
        <div class="col-md-2">
            <select id="selectbasic" name="filtro_reg" class="form-control">
                <option value="codigo">Código</option>
                <option value="nome">Nome</option>
            </select>
        </div>
        <div class="col-md-5">
            <input id="textinput" name="filtro_nome" type="text" autocomplete="off" class="form-control input-md" placeholder="Digire o código ou nome conforme o filtro">
        </div>
    </div>


        <div class="row">
          <label class="col-md-1 control-label" for="selectbasic">Nivel de usuário</label>
          <div class="col-md-4">
            <select id="selectbasic" name="filtro_tipo" class="form-control">
              <option value="all">- Todos -</option>
              <option value="adm">Administrador</option>
              <option value="user">Usuários</option>
            </select>
          </div>
        </div>
        <div class="form-row">
            <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" type="submit" name="filtrar" value="Aplicar"> </input><button class="btn btn-danger form-btn" type="reset">Limpar </button></div>
        </div>

</fieldset>
</form>

    <?php

        if($_POST['filtrar']){



            //include "includes/config.php";

            if($_POST['filtro_tipo'] == "all"){
                if($_POST['filtro_reg'] == "codigo"){
                    if($_POST['filtro_nome'] == ""){
                        $sql = "SELECT * FROM usuarios ORDER BY usuario_id ASC";
                        $res = mysqli_query($conn, $sql);
                    }else{
                        $sql = "SELECT * FROM usuarios WHERE usuario_id = '$_POST[filtro_nome]' ORDER BY usuario_id ASC";
                        $res = mysqli_query($conn, $sql);
                    }
                }else{
                    if($_POST['filtro_nome'] == ""){
                        $sql = "SELECT * FROM usuarios ORDER BY usuario_id ASC";
                        $res = mysqli_query($conn, $sql);
                    }else{
                        $sql = "SELECT * FROM usuarios WHERE nome like '%$_POST[filtro_nome]%' ORDER BY usuario_id ASC";
                        $res = mysqli_query($conn, $sql);
                    }
                }
            }elseif ($_POST['filtro_tipo'] == "adm"){
                if($_POST['filtro_reg'] == "codigo"){
                    if($_POST['filtro_nome'] == ""){
                        $sql = "SELECT * FROM usuarios WHERE nivel_usuario = '2' ORDER BY usuario_id ASC";
                        $res = mysqli_query($conn, $sql);
                    }else{
                        $sql = "SELECT * FROM usuarios WHERE usuario_id = '$_POST[filtro_nome]' and nivel_usuario = '2' ORDER BY usuario_id ASC";
                        $res = mysqli_query($conn, $sql);
                    }
                }else{
                   if($_POST['filtro_nome'] == ""){
                       $sql = "SELECT * FROM usuarios WHERE nivel_usuario = '2' ORDER BY usuario_id ASC";
                       $res = mysqli_query($conn, $sql);
                   }else{
                       $sql = "SELECT * FROM usuarios WHERE nome like = '%$_POST[filtro_nome]%' and nivel_usuario = '2' ORDER BY usuario_id ASC";
                       $res = mysqli_query($conn, $sql);
                   }
                }
            }elseif ($_POST['filtro_tipo'] == "user"){
                if($_POST['filtro_reg'] == "codigo"){
                    if($_POST['filtro_nome'] == ""){
                        $sql = "SELECT * FROM cad_clientes WHERE tp_cliente = 'j' ORDER BY cd_cliente ASC";
                        $res = mysqli_query($conn, $sql);
                    }else{
                        $sql = "SELECT * FROM cad_clientes WHERE cd_cliente = '$_POST[filtro_nome]' and tp_cliente = 'j' ORDER BY cd_cliente ASC";
                        $res = mysqli_query($conn, $sql);
                    }
                }else{
                   if($_POST['filtro_nome'] == ""){
                       $sql = "SELECT * FROM cad_clientes WHERE tp_cliente = 'j' ORDER BY cd_cliente ASC";
                       $res = mysqli_query($conn, $sql);
                   }else{
                       $sql = "SELECT * FROM cad_clientes WHERE cli_nomerazao like = '%$_POST[filtro_nome]%' and tp_cliente = 'j' ORDER BY cd_cliente ASC";
                       $res = mysqli_query($conn, $sql);
                   }
                }
            }

            //Verificar se encontrou resultado na tabela "usuarios"
            if(($res) AND ($res->num_rows != 0)){
                ?>
                <hr>
                <table class="table table-striped table-bordered table-hover">
                    <legend>Resultado da busca:</legend>
                    <BR>
                    <thead>
                    <tr>
                        <th width="40">Cód</th>
                        <th width="300">Nome</th>
                        <th>Fone</th>
                        <th>Nivel</th>
                        <th>Ativo</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row_cli = mysqli_fetch_assoc($res)){
                        $id = $row_cli['usuario_id'];
                        $_SESSION['id'] = $id;
                        ?>
                        <tr>
                            <th><?php echo $row_cli['usuario_id']; ?></th>
                            <td><a href="altera_cadusuario.php?id=<?php echo $row_cli['usuario_id'] ?>"><?php echo $row_cli['nome']; ?></a></td>
                            <td><?php echo $row_cli['fone']; ?></td>
                            <td><?php if($row_cli['nivel_usuario'] == 0){ echo 'Usuários';} elseif($row_cli['nivel_usuario'] == 1){ echo 'Usuários';} elseif($row_cli['nivel_usuario'] == 2){ echo 'Admin';} ?></td>
                            <td><?php if($row_cli['ativado'] == 1){ echo 'Ativo';} else{ echo 'Desativado';} ?></td>
                        </tr>
                        <?php
                    }?>
                    </tbody>

                </table>
                <BR>
                <?php
            }else{
                echo "<div class='alert alert-danger' role='alert'>Nenhum registro encontrado!</div><BR>";

            }
        }
    ?>


</div>

</body>



</html>
