<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <script type="text/javascript" >

        function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('rua').value=("");
            document.getElementById('bairro').value=("");
            document.getElementById('cidade').value=("");
            document.getElementById('estado').value=("");

        }

        function meu_callback(conteudo) {
            if (!("erro" in conteudo)) {
                //Atualiza os campos com os valores.
                document.getElementById('rua').value=(conteudo.logradouro);
                document.getElementById('bairro').value=(conteudo.bairro);
                document.getElementById('cidade').value=(conteudo.localidade);
                $("#estado option").filter(function() {
		              return this.text == conteudo.uf; 
	              }).attr('selected', true);

            } //end if.
            else {
                //CEP não Encontrado.
                limpa_formulário_cep();
                alert("CEP não encontrado.");
            }
        }

        function pesquisacep(valor) {

            //Nova variável "cep" somente com dígitos.
            var cep = valor.replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if(validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    document.getElementById('rua').value="...";
                    document.getElementById('bairro').value="...";
                    document.getElementById('cidade').value="...";
                    document.getElementById('estado').value="...";


                    //Cria um elemento javascript.
                    var script = document.createElement('script');

                    //Sincroniza com o callback.
                    script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';

                    //Insere script no documento e carrega o conteúdo.
                    document.body.appendChild(script);

                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        };

    </script>



    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>


</head>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>


<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

  <form class="form-horizontal" action="#" method="post">
    <?php
      if (isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
      }
      if (isset($_SESSION['msgcad'])){
        echo $_SESSION['msgcad'];
        unset($_SESSION['msgcad']);
      }
    ?>
<fieldset>
<!-- Form Name -->

<legend>Cadastro de Clientes</legend>

    <?php

    $qry = " SELECT AUTO_INCREMENT as cd_cliente FROM information_schema.TABLES WHERE TABLE_SCHEMA = '" . $database_connect . "' AND TABLE_NAME = 'cad_clientes' ";
    $sql_id = mysqli_query($conn, $qry);
    $info = mysqli_fetch_array($sql_id);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_cliente" type="text" readonly value="<?php echo $info['cd_cliente']; ?>" class="form-control input-md">
        </div>

  </div>

<!-- Text input-->
<div class="row">
  <label class="col-md-1 control-label" for="textinput">Nome/Razão*</label>
  <div class="col-md-5">
  <input id="textinput" name="nomerazao" type="text" value="" placeholder="" autocomplete="off" required="" class="form-control input-md">
</div>
<label class="col-md-1 control-label" for="textinput">Fantasia*</label>
  <div class="col-md-4">
  <input id="textinput" name="fantasia" type="text" value="" placeholder="" autocomplete="off" class="form-control input-md">
  </div>
  </div>

  <!-- Select Basic -->
  <div class="row">
    <label class="col-md-1 control-label" for="selectbasic">Grupo</label>
    <div class="col-md-5">
      <select id="selectbasic" name="tp_pessoa" required class="form-control">
        <option label="- Selecione o tipo de pessoa -">/option>
        <option value="f">Pessoa Física</option>
        <option value="j">Pessoa Jurídica</option>
      </select>
    </div>
    <label class="col-md-1 control-label" for="textinput">Limite</label>
      <div class="col-md-4">
      <input id="textinput" name="limite" type="text" value="" placeholder="" autocomplete="off" class="form-control input-md">
      </div>
      </div>


  <div class="row">
    <label class="col-md-1 control-label" for="textinput">Fone 1</label>
    <div class="col-md-5">
    <input id="telefone" name="fone1" value="" type="text"  maxlength="14"  placeholder="DDD + Telefone" autocomplete="off" class="form-control input-md">
  </div>
  <label class="col-md-1 control-label" for="textinput">Fone 2</label>
    <div class="col-md-4">
    <input id="telefone" name="fone2" value="" type="text"  maxlength="14" placeholder="DDD + Telefone" class="form-control input-md">
    </div>
    </div>

    <div class="row">
      <label class="col-md-1 control-label" for="textinput">Fone 3</label>
      <div class="col-md-5">
      <input id="telefone" name="fone3" value="" type="text" maxlength="14" placeholder="DDD + Telefone" class="form-control input-md">
    </div>
    <label class="col-md-2 control-label" for="textinput">Data de Nascimento</label>
      <div class="col-md-3">
          <input type="text" class="form-control" id="datanascimento" name="nascimento" >
          <div class="input-group-addon">
              <span class="glyphicon glyphicon-th"></span>
          </div>
      </div>
      </div>

      <div class="row">
        <label class="col-md-1 control-label" for="textinput">Endereço</label>
        <div class="col-md-6">
        <input name="rua" id="rua" type="text" class="form-control input-md">
      </div>
      <label class="col-md-1 control-label" for="textinput">Numero</label>
        <div class="col-md-3">
        <input name="numero" type="text" class="form-control input-md">
        </div>
        </div>

        <div class="row">
          <label class="col-md-1 control-label" for="textinput">Bairro</label>
          <div class="col-md-5">
          <input name="bairro" id="bairro" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
        </div>
        <label class="col-md-1 control-label" for="textinput">Complem.</label>
          <div class="col-md-4">
          <input name="complemento" id="complemento" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          </div>

          <div class="row">
            <label class="col-md-1 control-label" for="textinput">Cidade</label>
            <div class="col-md-5">
            <input name="cidade" id="cidade" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
          </div>
          <label class="col-md-1 control-label" for="selectbasic">Estado</label>
          <div class="col-md-1">
            <select id="estado" name="estado" class="form-control">
                <?php
                $sql2 =  mysqli_query($conn, "SELECT * FROM estados order by sigla asc");
                while ($result = mysqli_fetch_array($sql2) )
                {
                    ?>
                    <option value="<?php echo $result['id_estado']; ?>"><?php echo $result['sigla']; ?></option>";
                <?php
                }
                ?>
            </select>
            </div>
            <label class="col-md-1 control-label" for="textinput">CEP</label>
            <div class="col-md-2">
            <input id="textinput" name="cep" id="cep" value="" type="text" onblur="pesquisacep(this.value);" class="form-control input-md">
          </div>
            </div>

            <div class="row">
              <label class="col-md-1 control-label" for="textinput">Email</label>
              <div class="col-md-5">
              <input id="textinput" name="email" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
            </div>
            <label class="col-md-2 control-label" for="textinput">Desconto Padrão (%)</label>
              <div class="col-md-1">
              <input id="textinput" name="descontopd" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
              </div>
              </div>

              <div class="row">
                <label class="col-md-1 control-label" for="textinput">CPF/CNPJ</label>
                <div class="col-md-5">
                <input id="textinput" name="cpfcnpj" value="" type="text" maxlength="14" placeholder="" autocomplete="off" class="form-control input-md">
              </div>
              <label class="col-md-1 control-label" for="textinput">Identidade</label>
                <div class="col-md-4">
                <input id="textinput" name="identidade" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                </div>
                </div>

                <div class="row">
                  <label class="col-md-1 control-label" for="textinput">Observação</label>
                  <div class="col-md-10">
                  <input id="textinput" name="obs" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                </div>
                  </div>

                  <div class="row">
                    <label class="col-md-1 control-label" for="textinput">Facebook</label>
                    <div class="col-md-9">
                    <input id="textinput" name="facebook" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                  </div>
                  <span class="input-group-btn">
                    <button class="btn btn-primary" type="button">Acessar</button>
                  </span>
                    </div>

                    <div class="row">
                      <label class="col-md-1 control-label" for="textinput">Instagram</label>
                      <div class="col-md-9">
                      <input id="textinput" name="instagram" value="" type="text" placeholder="" autocomplete="off" class="form-control input-md">
                    </div>
                    <span class="input-group-btn">
                      <button class="btn btn-primary" type="button">Acessar</button>
                    </span>
                      </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <input class="btn btn-danger form-btn" type="button" onclick="form.action='consulta_cliente.php'; form.submit()" value="Cancelar" ></div>
                      </div>

</fieldset>
</form>

    <?php

    include_once ("../../includes/config.php");

    if($_POST['gravar']){

        $codigo = $_POST["cd_cliente"];
        $nome = $_POST["nomerazao"];
        $fantasia = $_POST["fantasia"];
        $limite = $_POST["limite"];
        $fone1 = $_POST["fone1"];
        $fone2 = $_POST["fone2"];
        $fone3 = $_POST["fone3"];
        $nascimento = $_POST["nascimento"];
        $rua = $_POST["rua"];
        $numero = $_POST["numero"];
        $bairro = $_POST["bairro"];
        $complemento = $_POST["complemento"];
        $cidade = addslashes($_POST["cidade"]);
        $estado = $_POST["estado"];
        $cep = $_POST["cep"];
        $email = $_POST["email"];
        $desconto = $_POST["descontopd"];
        $cpfcnpj = $_POST["cpfcnpj"];
        $identidade = $_POST["identidade"];
        $observacao= $_POST["obs"];
        $site1 = $_POST["facebook"];
        $site2 = $_POST["instagram"];
        $tipo = $_POST['tp_pessoa'];

        $nascimento = date("Y-m-d");
        $nascimento = strtotime($nascimento);

        $rotina = "CADCLI";
        $codrotina = $codigo;
        $descricaolog = "Cadastro de cliente - $nome";

        if (!mysqli_query($conn, "INSERT INTO cad_clientes (cli_nomerazao, cli_fantasia, cli_limite, cli_fone1, cli_fone2, cli_fone3, cli_dtnasc, cli_endereco, cli_numero, cli_bairro, cli_complemento, cli_cidade, cli_estado, cli_cep, cli_email, cli_descpd, cli_cnpjcpf, cli_ident, cli_obs, cli_site1, cli_site2, tp_cliente)".
                 " VALUES('" . addslashes($nome) . "','" . addslashes($fantasia) . "','" . addslashes($limite) . "','" . $fone1 . "','" . $fone2 . "','" . $fone3 . "','" . $nascimento . "','" . addslashes($rua) . "','" . $numero . "','" . addslashes($bairro) . "','" . addslashes($complemento) . "',".
                 " '" . addslashes($cidade) . "','" . addslashes($estado) . "','" . $cep . "',lower('$email'),'" . $desconto . "','" . $cpfcnpj . "','" . $identidade . "','" . addslashes($observacao) . "','" . addslashes($site1) . "','" . addslashes($site2) . "','" . $tipo . "')"))
          {
            echo("Erro: " . mysqli_error($conn));
            echo '<script type="text/javascript">toastr.error("Erro ao cadastrar Cliente!")</script>';
          }else{
            echo '<script type="text/javascript">toastr.success("Cliente cadastrado com sucesso!")</script>';
            $sql1 = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
            header("Location: consulta_cliente.php");
        }
    }

    ?>
</div>

<script type="text/javascript">
    $('#datanascimento').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    });
</script>



</body>



</html>
