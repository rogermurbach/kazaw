<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>


<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
        function addFields(){
            var number = document.getElementById("numpagam").value;
            var container = document.getElementById("cont");
            while (container.hasChildNodes()) {
                container.removeChild(container.lastChild);
            }
            for (i=0;i<number;i++){
                container.appendChild(document.createTextNode("Parcela " + (i+1)));
                var input = document.createElement("input");
                input.type = "text";
                input.name = ("parc" + (i+1));
                input.class = "form-control input-md";
                container.appendChild(input);
                container.appendChild(document.createElement("br"));
            }
        }
</script>



<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Cadastro de Forma de Pagamento</legend>

    <?php

    $sql= mysqli_query($conn,"SELECT cd_pagamento FROM cond_pagamento order by cd_pagamento desc");
    $info = mysqli_fetch_array($sql);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_pagamento" type="text" readonly value="<?php $info = $info['cd_pagamento'] + 1; echo $info ?>" class="form-control input-md">
        </div>
  </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Descrição</label>
            <div class="col-md-5">
                <input id="textinput" name="descricao" type="text" value="" class="form-control input-md">
            </div>
        </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Parcelas</label>
            <div class="col-md-1">
                <input id="numpagam" name="parcelas" type="text" value="" class="form-control input-md">
            </div>
            <div class="col-md-2">
                <button id="disable" type="button" class="btn btn-info form-btn" data-toggle="modal" data-target="#myModal" onclick="addFields()">Gerar Prazos</button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Número de dias</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body" >

       
                        <div class="row justify-content-around">
                            
                            <div class="col-md-3" id="cont">
                            </div>
                            
                        </div>




                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Salvar</button>
                    </div>
                </div>

            </div>
        </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <button class="btn btn-danger form-btn" type="reset" href="">Cancelar </button></div>
                      </div>

</fieldset>
</form>

    <?php

    include_once ("../../includes/config.php");

    $codigo = $_POST["cd_pagamento"];
    $descricao = $_POST["descricao"];
    $numpag = $_POST['parcelas'];
    $parc1 = $_POST['parc1'];
    $parc2 = $_POST['parc2'];
    $parc3 = $_POST['parc3'];
    $parc4 = $_POST['parc4'];
    $parc5 = $_POST['parc5'];
    $parc6 = $_POST['parc6'];
    $parc7 = $_POST['parc7'];
    $parc8 = $_POST['parc8'];
    $parc9 = $_POST['parc9'];
    $parc10 = $_POST['parc10'];
    $parc11 = $_POST['parc11'];
    $parc12 = $_POST['parc12'];
    $parc13 = $_POST['parc13'];
    $parc14 = $_POST['parc14'];
    $parc15 = $_POST['parc15'];
    $parc16 = $_POST['parc16'];
    $parc17 = $_POST['parc17'];
    $parc18 = $_POST['parc18'];
    $parc19 = $_POST['parc19'];
    $parc20 = $_POST['parc20'];
    $parc21 = $_POST['parc21'];
    $parc22 = $_POST['parc22'];
    $parc23 = $_POST['parc23'];
    $parc24 = $_POST['parc24'];


    if($_POST['gravar']){

        $rotina = "CADFPA";
        $codrotina = $codigo;
        $descricaolog = "Cadastro de forma pagto - $descricao";

        $sql = mysqli_query($conn, "INSERT INTO cond_pagamento (con_descricao, con_numpag, con_pag1, con_pag2, con_pag3, con_pag4, con_pag5, con_pag6, con_pag7, con_pag8, con_pag9, con_pag10, con_pag11, con_pag12, con_pag13, con_pag14, con_pag15, con_pag16, con_pag17, con_pag18, con_pag19, con_pag20, con_pag21, con_pag22, con_pag23, con_pag24) VALUES (UPPER('$descricao'), '$numpag', '$parc1', '$parc2', '$parc3', '$parc4', '$parc5', '$parc6', '$parc7', '$parc8', '$parc9', '$parc10', '$parc11', '$parc12', '$parc13', '$parc14', '$parc15', '$parc16', '$parc17', '$parc18', '$parc19', '$parc20', '$parc21', '$parc22', '$parc23', '$parc24')");

        if ($sql) { // verificação para saber se foi cadastrado
            $sqllog = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
            $_SESSION['msgcad'] = "<BR><div class='alert alert-success'>Forma de pagamento cadastrada com sucesso!</div>";
            header("Location: formas_pagamento.php");


        } else { // Caso dê erro
            $_SESSION['msg'] = "<BR><div class='alert alert-danger'>Erro ao cadastrar a forma de pagamento!</div>"; mysqli_error($sql);
            //$_SESSION["msgcad"] ="Não foi possível incluir " .mysqli_error($sql);
            header("Location: cadastro_forma_pagamento.php");

        }


    }

    ?>


</div>
<script type="text/javascript">
    $('#datacadastro').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    });
</script>

</body>



</html>
