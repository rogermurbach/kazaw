<?php
session_start();
include("../../includes/seguranca.php");
protegePagina();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" manifest="kazaw.appcache">

<head>

    <?php
    $titulo = "KAZA W";
    include_once ("../../includes/header.php");
    ?>

    <style type="text/css">

    .form-control-ddd {
        display: block;
        width: 20%;
        padding: .375rem .75rem;
        font-size: 1rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ced4da;
        border-radius: .25rem;
        transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out
    }
    </style>

    <script>
        function callconfirm(id)
        {
            bootbox.confirm({
                message: "TEM CERTEZA QUE DESEJA EXCLUIR ESTE REGISTRO?",
                buttons: {
                    confirm: {
                        label: 'SIM',
                        className: 'btn-success',
                        callback: function(){
                            window.location.href = "deleta_formapagto.php"
                        }
                    },
                    cancel: {
                        label: 'NÃO',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    // console.log('This was logged in the callback: ' + result);
                    if(result == true){
                        window.location.href = "deleta_formapagto.php?id="+id
                    }else{

                    }

                }
            });

        }
    </script>

<body style="background-image:url(&quot;../../assets/img/kazaw_logo2.png&quot;); background-size: cover; background-repeat: no-repeat">

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<script type="text/javascript">
    num = document.getElementById('numpagam').value;

    $(document).ready(function() {
        $("#disable").click(function (){
            // desabilitando o campo
            alert(num);
            //alert(document.getElementsByName('parcelas').value)


                $('#par1').attr("disabled", true);
                // mudando a cor do campo
                $('#par1').css("background-color", "#cccccc");



        });
    });
</script>



<?php
include('../../includes/topo_menu.php');
require_once('../../includes/config.php');
?>

<div class="ctd" id="conteudo">

    <form class="form-horizontal" action="" method="post">
        <?php
        if (isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['msgcad'])){
            echo $_SESSION['msgcad'];
            unset($_SESSION['msgcad']);
        }
        ?>
    <fieldset>

<!-- Form Name -->

<legend>Cadastro de Forma de Pagamento</legend>

    <?php

    $qry = "select * from cond_pagamento where cd_pagamento=$_GET[id]";
    $sql = mysqli_query($conn, $qry);
    $info = mysqli_fetch_object($sql);

    ?>


        <div id="cid_6" class="form-input-wide">
          <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:10px;margin-bottom:15px">
          </div>
        </div>

      <div class="row" >
        <label class="col-md-1 control-label" for="textinput">Código</label>
        <div class="col-md-1">
        <input id="textinput" name="cd_pagamento" type="text" readonly value="<?php echo $info->cd_pagamento; ?>" class="form-control input-md">
        </div>
  </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Descrição</label>
            <div class="col-md-5">
                <input id="textinput" name="descricao" type="text" value="<?php echo $info->con_descricao; ?>" class="form-control input-md">
            </div>
        </div>

        <div class="row">
            <label class="col-md-1 control-label" for="textinput">Parcelas</label>
            <div class="col-md-1">
                <input id="numpagam" name="parcelas" type="text" value="<?php echo $info->con_numpag; ?>" class="form-control input-md">
            </div>
            <div class="col-md-2">
                <button id="disable" type="button" class="btn btn-info form-btn" data-toggle="modal" data-target="#myModal">Gerar Prazos</button>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Número de dias</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <div class="row justify-content-around">
                            <label for="textinput">Parcela 1</label>
                            <div class="col-md-1">
                                <input id="par1" name="parc1" type="text" value="<?php echo $info->con_pag1; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 2</label>
                            <div class="col-md-1">
                                <input id="par2" name="parc2" type="text" value="<?php echo $info->con_pag2; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 3</label>
                            <div class="col-md-1">
                                <input id="par3" name="parc3" type="text" value="<?php echo $info->con_pag3; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 4</label>
                            <div class="col-md-1">
                                <input id="par4" name="parc4" type="text" value="<?php echo $info->con_pag4; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 5</label>
                            <div class="col-md-1">
                                <input id="par5" name="parc5" type="text" value="<?php echo $info->con_pag5; ?>" class="form-control input-md">
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <label for="textinput">Parcela 6</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc6" type="text" value="<?php echo $info->con_pag6; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 7</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc7" type="text" value="<?php echo $info->con_pag7; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 8</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc8" type="text" value="<?php echo $info->con_pag8; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 9</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc9" type="text" value="<?php echo $info->con_pag9; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 10</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc10" type="text" value="<?php echo $info->con_pag10; ?>" class="form-control input-md">
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <label for="textinput">Parcela 11</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc11" type="text" value="<?php echo $info->con_pag11; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 12</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc12" type="text" value="<?php echo $info->con_pag12; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 13</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc13" type="text" value="<?php echo $info->con_pag13; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 14</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc14" type="text" value="<?php echo $info->con_pag14; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 15</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc15" type="text" value="<?php echo $info->con_pag15; ?>" class="form-control input-md">
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <label for="textinput">Parcela 16</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc16" type="text" value="<?php echo $info->con_pag16; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 17</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc17" type="text" value="<?php echo $info->con_pag17; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 18</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc18" type="text" value="<?php echo $info->con_pag18; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 19</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc19" type="text" value="<?php echo $info->con_pag19; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 20</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc20" type="text" value="<?php echo $info->con_pag20; ?>" class="form-control input-md">
                            </div>
                        </div>
                        <div class="row justify-content-around">
                            <label for="textinput">Parcela 21</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc21" type="text" value="<?php echo $info->con_pag21; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 22</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc22" type="text" value="<?php echo $info->con_pag22; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 23</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc23" type="text" value="<?php echo $info->con_pag23; ?>" class="form-control input-md">
                            </div>
                            <label for="textinput">Parcela 24</label>
                            <div class="col-md-1">
                                <input id="textinput" name="parc24" type="text" value="<?php echo $info->con_pag24; ?>" class="form-control input-md">
                            </div>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Salvar</button>
                    </div>
                </div>

            </div>
        </div>

                      <div id="cid_6" class="form-input-wide">
                        <div data-component="divider" style="border-bottom:1px solid #e6e6e6;height:1px;margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px">
                        </div>
                      </div>
                    </li>


                      <div class="form-row">
                          <div class="col-md-12 content-center" style="margin-left:0px;margin-right:0px;margin-top:15px;margin-bottom:15px"><input class="btn btn-primary form-btn" name="gravar" type="submit" value="Salvar" > <button class="btn btn-secondary form-btn" type="button" onclick="callconfirm('<?php echo $info->cd_pagamento ?>')" >Excluir </button> <input class="btn btn-danger form-btn" type="button" onclick="form.action='formas_pagamento.php'; form.submit()" value="Cancelar" > </div>
                      </div>

</fieldset>
</form>

    <?php

    include_once ("includes/config.php");

    $codigo = $_POST["cd_pagamento"];
    $descricao = $_POST["descricao"];
    $numpag = $_POST['parcelas'];
    $parc1 = $_POST['parc1'];
    $parc2 = $_POST['parc2'];
    $parc3 = $_POST['parc3'];
    $parc4 = $_POST['parc4'];
    $parc5 = $_POST['parc5'];
    $parc6 = $_POST['parc6'];
    $parc7 = $_POST['parc7'];
    $parc8 = $_POST['parc8'];
    $parc9 = $_POST['parc9'];
    $parc10 = $_POST['parc10'];
    $parc11 = $_POST['parc11'];
    $parc12 = $_POST['parc12'];
    $parc13 = $_POST['parc13'];
    $parc14 = $_POST['parc14'];
    $parc15 = $_POST['parc15'];
    $parc16 = $_POST['parc16'];
    $parc17 = $_POST['parc17'];
    $parc18 = $_POST['parc18'];
    $parc19 = $_POST['parc19'];
    $parc20 = $_POST['parc20'];
    $parc21 = $_POST['parc21'];
    $parc22 = $_POST['parc22'];
    $parc23 = $_POST['parc23'];
    $parc24 = $_POST['parc24'];


    if($_POST['gravar']){

        $rotina = "ALTFPA";
        $codrotina = $codigo;
        $descricaolog = "Alteração de forma pagto - $descricao";


        $sql = mysqli_query($conn, "UPDATE cond_pagamento SET con_descricao = UPPER('$descricao'), con_numpag = '$numpag', con_pag1 = '$parc1', con_pag2 = '$parc2', con_pag3 = '$parc3', con_pag4 = '$parc4', con_pag5 = '$parc5', con_pag6 = '$parc6', con_pag7 = '$parc7', con_pag8 = '$parc8', con_pag9 = '$parc9', con_pag10 = '$parc10', con_pag11 = '$parc11', con_pag12 = '$parc12', con_pag13 = '$parc13', con_pag14 = '$parc14', con_pag15 = '$parc15', con_pag16 = '$parc16', con_pag17 = '$parc17', con_pag18 = '$parc18', con_pag19 = '$parc19', con_pag20 = '$parc20', con_pag21 = '$parc21', con_pag22 = '$parc22', con_pag23 = '$parc23', con_pag24 = '$parc24' where cd_pagamento = '$codigo'");
        if ($sql) { // verificação para saber se foi cadastrado
            $sqllog = mysqli_query($conn, "INSERT INTO log_geral (log_data, log_rotina, log_codrotina, log_descricao) values (now(), '$rotina', '$codrotina', '$descricaolog')");
            $_SESSION['msgcad'] = "<BR><div class='alert alert-success'>Forma de pagamento alterada com sucesso!</div>";
            header("Location: formas_pagamento.php");


        } else { // Caso dê erro
            $_SESSION['msg'] = "<BR><div class='alert alert-danger'>Erro ao alterar a forma de pagamento!</div>"; mysqli_error($sql);
            //$_SESSION["msgcad"] ="Não foi possível incluir " .mysqli_error($sql);
            header("Location: cadastro_forma_pagamento.php");

        }


    }

    ?>


</div>
<script type="text/javascript">
    $('#datacadastro').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    });
</script>

</body>



</html>
