<?php

session_start();  // Inicia a session

include "includes/config.php";

$usuario = $_POST['usuario'];
$senha = $_POST['senha'];
$validaSempre = true;

$senhacrip = md5($senha);

if((!$usuario) || (!$senha)){

	$_SESSION['msg'] = "<div class='alert alert-danger'>Todos os campos devem ser preenchidos!</div>";
			header("Location: login.php");

}else{

	//$senhacrip = md5($senha);

	$sql = mysqli_query($conn, "SELECT * FROM usuarios WHERE usuario='{$usuario}' AND senha='{$senhacrip}'");
	//$login_check = mysqli_num_rows($sql);

	$resultado = mysqli_fetch_assoc($sql);

	// Verifica se encontrou algum registro
	if (empty($resultado)) {
	  // Nenhum registro foi encontrado => o usuário é inválido

	  $_SESSION['msg'] = "<div class='alert alert-danger'>Login ou senha incorretos!</div>";
	  header("Location: login.php");

	} else {
		if($resultado['ativado'] == 0){
			$_SESSION['msg'] = "<div class='alert alert-danger'>Login bloqueado!</div>";
			header("Location: login.php");
		}else{
			// Definimos dois valores na sessão com os dados do usuário
			$_SESSION['usuarioID'] = $resultado['usuario_id']; // Pega o valor da coluna 'id do registro encontrado no MySQL
			$_SESSION['usuarioNome'] = $resultado['nome']; // Pega o valor da coluna 'nome' do registro encontrado no MySQL
		
			// Verifica a opção se sempre validar o login
			if ($validaSempre == true) {
				// Definimos dois valores na sessão com os dados do login
				$_SESSION['usuarioLogin'] = $usuario;
				$_SESSION['usuarioSenha'] = $senhacrip;
				$_SESSION['nivel_usuario'] = $resultado['nivel_usuario'];
			}
			mysqli_query($conn, "UPDATE usuarios SET data_ultimo_login = now() WHERE usuario_id ='{$resultado['usuario_id']}'");
			header("Location: area_restrita.php");
		}
	}
}

?>
