-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 07/12/2020 às 19:56
-- Versão do servidor: 5.7.26
-- Versão do PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `kazaw_bd`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `ajuste_estoque`
--

CREATE TABLE `ajuste_estoque` (
  `cd_ajuste` int(11) NOT NULL,
  `aju_data` date DEFAULT NULL,
  `aju_motivo` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `ajuste_estoque`
--

INSERT INTO `ajuste_estoque` (`cd_ajuste`, `aju_data`, `aju_motivo`) VALUES
(20, '2018-10-17', 'tes'),
(21, '2018-10-17', 'tess'),
(22, '2018-10-17', 'rrrr'),
(23, '2018-10-17', 'eer'),
(24, '2018-10-17', 'dsdsd'),
(25, '2018-10-17', 'tete'),
(26, '2018-10-18', '6543'),
(27, '2018-10-18', '44'),
(28, '2018-10-18', 'kj'),
(29, '2018-10-18', 'hfhf'),
(30, '2018-10-18', 'nnn'),
(31, '2018-10-18', 'nn'),
(32, '2018-10-18', 'wdw'),
(33, '2018-10-22', 'werwe'),
(34, '2018-10-22', 'sadsd'),
(35, '2018-10-22', 'sdsd'),
(36, '2018-10-22', 'dfdf'),
(37, '2018-10-22', 'dsadsa'),
(38, '2018-10-22', 'sds'),
(39, '2018-10-22', 'sd'),
(40, '2018-10-22', 'asas'),
(41, '2018-10-22', 'eee'),
(42, '2018-10-22', 'sdsd'),
(43, '2018-10-22', 'sdsdsdfdf'),
(44, '2018-10-22', 'dsdds'),
(45, '2018-10-22', 'ff'),
(46, '2018-10-22', 'sdsdsd'),
(47, '2018-10-22', 'sdsd'),
(48, '2018-10-22', 'dsdsd'),
(49, '2018-10-22', 'dsdsd'),
(50, '2018-10-22', 'dsdsd'),
(51, '2018-10-22', 'dsdsd'),
(52, '2018-10-22', 'sdsd'),
(53, '2018-10-22', 'sdsd'),
(54, '2018-10-22', 'sdsd'),
(55, '2018-10-22', 'sdsd'),
(56, '2018-10-22', 'rrrr'),
(57, '2018-10-22', 'rrrr'),
(58, '2018-10-22', 'rrrr'),
(59, '2018-10-22', 'rrrr'),
(60, '2018-10-22', 'dfdfd'),
(61, '2018-10-22', 'dfdfd'),
(62, '2018-10-22', 'dfdfd'),
(63, '2018-10-22', 'dfdfd'),
(64, '2018-10-22', 'dfdfd'),
(65, '2018-10-22', 'fvf'),
(66, '2018-10-22', 'fvf'),
(67, '2018-10-22', 'fvf'),
(68, '2018-10-22', 'fvf'),
(69, '2018-10-22', 'fvf'),
(70, '2018-10-22', 'fvf'),
(71, '2018-10-22', 'fvf'),
(72, '2018-10-22', 'fvf'),
(73, '2018-10-22', 'fvf'),
(74, '2018-10-22', 'srre'),
(75, '2018-10-22', 'srre'),
(76, '2018-10-22', 'srre'),
(77, '2018-10-22', 'srre'),
(78, '2018-10-22', 'cxcxc'),
(79, '2018-10-22', 'cxcxc'),
(80, '2018-10-22', 'cxcxc'),
(81, '2018-10-22', 'df'),
(82, '2018-10-22', 'df'),
(83, '2018-10-22', 'df'),
(84, '2018-10-31', 'hjllll'),
(85, '2018-10-31', 'hjllll'),
(86, '2018-10-31', 'hjllll'),
(87, '2018-10-31', 'asas'),
(88, '2018-10-31', 'asas'),
(89, '2018-10-31', 'asas'),
(90, '2018-10-31', 'sdfdff'),
(91, '2018-10-31', 'sdfdff'),
(92, '2018-10-31', 'sdfdff'),
(93, '2018-10-31', 'sdfdff'),
(94, '2018-10-31', 'sdfdff'),
(95, '2018-10-31', 'sdfdff'),
(96, '2018-10-31', 'sdfdff'),
(97, '2018-10-31', 'sdfdff'),
(98, '2018-10-31', 'teste'),
(99, '2018-10-31', 'teste'),
(100, '2018-10-31', 'teste'),
(101, '2018-10-31', 'tetete'),
(102, '2018-10-31', 'tetete'),
(103, '2018-11-01', 'dx'),
(104, '2018-11-22', 'te'),
(105, '2018-11-22', 'te'),
(106, '2018-11-22', 'te'),
(107, '2018-11-22', 'taeee'),
(108, '2018-11-22', 'taeee');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_clientes`
--

CREATE TABLE `cad_clientes` (
  `cd_cliente` int(10) NOT NULL,
  `cli_nomerazao` text NOT NULL,
  `cli_fantasia` text,
  `cli_limite` decimal(10,2) DEFAULT '0.00',
  `cli_fone1` varchar(15) DEFAULT NULL,
  `cli_fone2` varchar(15) DEFAULT NULL,
  `cli_fone3` varchar(15) DEFAULT NULL,
  `cli_dtnasc` varchar(200) DEFAULT NULL,
  `cli_endereco` text,
  `cli_numero` varchar(10) DEFAULT NULL,
  `cli_bairro` text,
  `cli_complemento` text,
  `cli_cidade` text,
  `cli_estado` varchar(50) DEFAULT NULL,
  `cli_cep` varchar(9) DEFAULT NULL,
  `cli_email` text,
  `cli_descpd` varchar(4) DEFAULT '0',
  `cli_cnpjcpf` varchar(50) DEFAULT NULL,
  `cli_ident` varchar(50) DEFAULT NULL,
  `cli_obs` text,
  `cli_site1` text,
  `cli_site2` text,
  `tp_cliente` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `cad_clientes`
--

INSERT INTO `cad_clientes` (`cd_cliente`, `cli_nomerazao`, `cli_fantasia`, `cli_limite`, `cli_fone1`, `cli_fone2`, `cli_fone3`, `cli_dtnasc`, `cli_endereco`, `cli_numero`, `cli_bairro`, `cli_complemento`, `cli_cidade`, `cli_estado`, `cli_cep`, `cli_email`, `cli_descpd`, `cli_cnpjcpf`, `cli_ident`, `cli_obs`, `cli_site1`, `cli_site2`, `tp_cliente`) VALUES
(10, 'Roger Murbach', '', '10000.00', '19994545585', '', '', '1987-07-20', 'Rua Guaianazes', '1626', 'Jardim Santa Rita de Cássia', '', 'Santa Bárbara D\'Oeste', '', '13457093', 'rogermf@msn.com', '5', '35450322801', '401141470', '', '', '', 'f'),
(11, 'David Pereira Vilela', '', '100000.00', '', '', '', '1989-04-10', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', 'f'),
(14, 'Antonio da Costa', '', '1000.00', '11212121', '', '', '1607299200', 'Rua Luiz Adami', '333', 'Vila Medon', '', 'Americana', '26', '13465220', 'antonio@gmail.com', '3', '444555', '66788', 'teste', '', '', 'f');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_empresa`
--

CREATE TABLE `cad_empresa` (
  `cd_empresa` int(11) NOT NULL,
  `emp_nomerazao` text,
  `emp_fantasia` text,
  `emp_fone1` varchar(20) DEFAULT NULL,
  `emp_fone2` varchar(20) DEFAULT NULL,
  `emp_fone3` varchar(20) DEFAULT NULL,
  `emp_endereco` text,
  `emp_numero` int(11) DEFAULT NULL,
  `emp_bairro` text,
  `emp_complemento` text,
  `emp_cidade` text,
  `emp_estado` int(11) DEFAULT NULL,
  `emp_cep` int(11) DEFAULT NULL,
  `emp_email` text,
  `emp_cnpjcpf` varchar(30) DEFAULT NULL,
  `emp_ident` varchar(25) DEFAULT NULL,
  `emp_obs` text,
  `emp_site1` text,
  `emp_site2` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `cad_empresa`
--

INSERT INTO `cad_empresa` (`cd_empresa`, `emp_nomerazao`, `emp_fantasia`, `emp_fone1`, `emp_fone2`, `emp_fone3`, `emp_endereco`, `emp_numero`, `emp_bairro`, `emp_complemento`, `emp_cidade`, `emp_estado`, `emp_cep`, `emp_email`, `emp_cnpjcpf`, `emp_ident`, `emp_obs`, `emp_site1`, `emp_site2`) VALUES
(1, 'KAZAW TAPETES', 'KAZAW', '111111111', NULL, NULL, 'RUA TALAIA', 233, 'CAMBUI', '', 'CAMPINAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `cad_fornecedores`
--

CREATE TABLE `cad_fornecedores` (
  `cd_fornecedor` int(10) NOT NULL,
  `for_nomerazao` text NOT NULL,
  `for_fantasia` text,
  `for_fone1` varchar(15) DEFAULT NULL,
  `for_fone2` varchar(15) DEFAULT NULL,
  `for_fone3` varchar(15) DEFAULT NULL,
  `for_dtabertura` varchar(200) DEFAULT NULL,
  `for_endereco` text,
  `for_numero` varchar(10) DEFAULT '0',
  `for_bairro` text,
  `for_complemento` text,
  `for_cidade` text,
  `for_estado` varchar(50) DEFAULT NULL,
  `for_cep` varchar(9) DEFAULT NULL,
  `for_email` text,
  `for_cnpjcpf` varchar(50) DEFAULT NULL,
  `for_ident` varchar(50) DEFAULT NULL,
  `for_obs` text,
  `for_site1` text,
  `for_site2` text,
  `tp_fornecedor` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `cad_fornecedores`
--

INSERT INTO `cad_fornecedores` (`cd_fornecedor`, `for_nomerazao`, `for_fantasia`, `for_fone1`, `for_fone2`, `for_fone3`, `for_dtabertura`, `for_endereco`, `for_numero`, `for_bairro`, `for_complemento`, `for_cidade`, `for_estado`, `for_cep`, `for_email`, `for_cnpjcpf`, `for_ident`, `for_obs`, `for_site1`, `for_site2`, `tp_fornecedor`) VALUES
(7, 'COLUMBIA', 'teste', '111', '111', '111', '1607299200', 'sadasd', '2', 'asdsad', 'sad', 'asdsa', '6', '1111', 'sdasd@glo.ocm', '1234', '1234', 'lala', '', '', 'j');

-- --------------------------------------------------------

--
-- Estrutura para tabela `cond_pagamento`
--

CREATE TABLE `cond_pagamento` (
  `cd_pagamento` int(11) NOT NULL,
  `con_descricao` text,
  `con_numpag` int(11) DEFAULT NULL,
  `con_pag1` int(11) DEFAULT '0',
  `con_pag2` int(11) DEFAULT '0',
  `con_pag3` int(11) DEFAULT '0',
  `con_pag4` int(11) DEFAULT '0',
  `con_pag5` int(11) DEFAULT '0',
  `con_pag6` int(11) DEFAULT '0',
  `con_pag7` int(11) DEFAULT '0',
  `con_pag8` int(11) DEFAULT '0',
  `con_pag9` int(11) DEFAULT '0',
  `con_pag10` int(11) DEFAULT '0',
  `con_pag11` int(11) DEFAULT '0',
  `con_pag12` int(11) DEFAULT '0',
  `con_pag13` int(11) DEFAULT '0',
  `con_pag14` int(11) DEFAULT '0',
  `con_pag15` int(11) DEFAULT '0',
  `con_pag16` int(11) DEFAULT '0',
  `con_pag17` int(11) DEFAULT '0',
  `con_pag18` int(11) DEFAULT '0',
  `con_pag19` int(11) DEFAULT '0',
  `con_pag20` int(11) DEFAULT '0',
  `con_pag21` int(11) DEFAULT '0',
  `con_pag22` int(11) DEFAULT '0',
  `con_pag23` int(11) DEFAULT '0',
  `con_pag24` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `cond_pagamento`
--

INSERT INTO `cond_pagamento` (`cd_pagamento`, `con_descricao`, `con_numpag`, `con_pag1`, `con_pag2`, `con_pag3`, `con_pag4`, `con_pag5`, `con_pag6`, `con_pag7`, `con_pag8`, `con_pag9`, `con_pag10`, `con_pag11`, `con_pag12`, `con_pag13`, `con_pag14`, `con_pag15`, `con_pag16`, `con_pag17`, `con_pag18`, `con_pag19`, `con_pag20`, `con_pag21`, `con_pag22`, `con_pag23`, `con_pag24`) VALUES
(1, 'A VISTA', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, '28DDL', 1, 28, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, '28/45DDL', 2, 28, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, '28/35/42DDL', 3, 28, 35, 42, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, '30DDL', 1, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(6, '30/60/90DDL', 3, 30, 60, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, '15 DIAS', 1, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contas_pagar`
--

CREATE TABLE `contas_pagar` (
  `cd_conpag` int(11) NOT NULL,
  `cd_condpag` int(11) DEFAULT NULL,
  `conpag_controle` int(11) DEFAULT NULL,
  `conpag_nf` int(11) DEFAULT NULL,
  `conpag_duplicata` varchar(100) DEFAULT NULL,
  `conpag_datavecto` date DEFAULT NULL,
  `conpag_valor` decimal(10,2) DEFAULT NULL,
  `conpag_multa` double DEFAULT NULL,
  `conpag_juros` double DEFAULT NULL,
  `conpag_obs` text,
  `conpag_parcela` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `contas_pagar`
--

INSERT INTO `contas_pagar` (`cd_conpag`, `cd_condpag`, `conpag_controle`, `conpag_nf`, `conpag_duplicata`, `conpag_datavecto`, `conpag_valor`, `conpag_multa`, `conpag_juros`, `conpag_obs`, `conpag_parcela`) VALUES
(1063430, 1, 5, 46464, '46464/1', '2018-11-07', '1717.00', NULL, NULL, NULL, 1),
(1063431, 1, 61, 555, '555/1', '2018-11-10', '3.00', NULL, NULL, NULL, 1),
(1063432, 1, 62, 123456, '123456/1', '2018-11-10', '12.00', NULL, NULL, NULL, 1),
(1063433, 1, 63, 12, '12/1', '2018-11-10', '33.00', NULL, NULL, NULL, 1),
(1063434, 4, 64, 9984, '9984/1', '2018-11-11', '2299.50', NULL, NULL, NULL, 1),
(1063435, 4, 64, 9984, '9984/2', '2018-11-28', '2299.50', NULL, NULL, NULL, 2),
(1063436, 5, 65, 11111, '11111/1', '2018-10-14', '11.00', NULL, NULL, NULL, 1),
(1063437, 1, 66, 11, '11/1', '2018-11-11', '122.00', NULL, NULL, NULL, 1),
(1063438, 1, 67, 333, '333/1', '2018-11-11', '33.00', NULL, NULL, NULL, 1),
(1063439, 1, 68, 1111122, '1111122/1', '2018-11-11', '333.00', NULL, NULL, NULL, 1),
(1063440, 5, 69, 999, '999/1', '2018-10-14', '78.00', NULL, NULL, NULL, 1),
(1063441, 8, 70, 2345, '2345/1', '2018-11-12', '83.25', NULL, NULL, NULL, 1),
(1063442, 8, 70, 2345, '2345/2', '2018-11-19', '83.25', NULL, NULL, NULL, 2),
(1063443, 8, 70, 2345, '2345/3', '2018-11-26', '83.25', NULL, NULL, NULL, 3),
(1063444, 8, 70, 2345, '2345/4', '2018-12-10', '83.25', NULL, NULL, NULL, 4),
(1063445, 1, 92, 1114444, '1114444/1', '2018-10-16', '3444.00', NULL, NULL, NULL, 1),
(1063446, 1, 94, 1444, '1444/1', '2018-10-16', '444.00', NULL, NULL, NULL, 1),
(1063447, 2, 98, 4444, '4444/1', '2018-11-13', '444.00', NULL, NULL, NULL, 1),
(1063448, 1, 99, 222, '222/1', '2018-10-16', '333.00', NULL, NULL, NULL, 1),
(1063449, 1, 100, 78, '78/1', '2018-10-16', '555.00', NULL, NULL, NULL, 1),
(1063450, 1, 101, 888, '888/1', '2018-10-17', '666.00', NULL, NULL, NULL, 1),
(1063457, NULL, 103, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(1063466, 3, 104, 58785, '58785/1', '2018-11-15', '1000.00', NULL, NULL, NULL, 1),
(1063467, 3, 104, 58785, '58785/2', '2018-12-15', '500.00', NULL, NULL, NULL, 2),
(1063468, 3, 105, 4545, '4545/1', '2018-11-28', '333.00', NULL, NULL, NULL, 1),
(1063469, 3, 105, 4545, '4545/2', '2018-12-15', '333.00', NULL, NULL, NULL, 2),
(1063470, 1, 106, 3445, '3445/1', '2018-10-31', '44.00', NULL, NULL, NULL, 1),
(1063471, 1, 107, 3333, '3333/1', '2018-10-31', '3.00', NULL, NULL, NULL, 1),
(1063472, 2, 109, 22222, '22222/1', '2019-10-05', '120.00', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `entrada_produto`
--

CREATE TABLE `entrada_produto` (
  `cd_entrada` int(11) NOT NULL,
  `ent_nf` varchar(10) DEFAULT NULL,
  `ent_data` date DEFAULT NULL,
  `ent_valornf` double DEFAULT NULL,
  `ent_fornecedor` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `entrada_produto`
--

INSERT INTO `entrada_produto` (`cd_entrada`, `ent_nf`, `ent_data`, `ent_valornf`, `ent_fornecedor`) VALUES
(1, '123', '2018-10-08', 350, 1),
(2, '1222', '2018-10-08', 100, 7),
(3, '111', '2018-10-08', 1111, 7),
(4, '11', '2018-10-08', 111, 7),
(5, '11', '2018-10-08', 111, 7),
(6, '11', '2018-10-08', 111, 7),
(7, '122', '2018-10-08', 111, 7),
(8, '1111', '2018-10-08', 111, 7),
(9, '11', '2018-10-08', 111, 7),
(10, '111', '2018-10-08', 111, 7),
(11, '111', '2018-10-08', 11, 7),
(12, '1111', '2018-10-08', 11, 7),
(13, '11', '2018-10-08', 11, 7),
(14, '111', '2018-10-08', 11, 7),
(15, '111', '2018-10-08', 11, 7),
(16, '11', '2018-10-08', 11, 7),
(17, '11', '2018-10-08', 111, 7),
(18, '11', '2018-10-08', 11, 7),
(19, '11', '2018-10-08', 11, 7),
(20, '111', '2018-10-08', 1111, 7),
(21, '111122', '2018-10-08', 33333, 7),
(22, '111', '2018-10-08', 1223, 7),
(23, '444', '2018-10-08', 444, 7),
(24, '1233', '2018-10-08', 444, 7),
(25, '1234', '2018-10-08', 3432, 7),
(26, '111', '2018-10-08', 11111, 7),
(27, '1111', '2018-10-08', 1111, 7),
(28, '11', '2018-10-08', 1, 7),
(29, '11', '2018-10-08', 11, 7),
(30, '111', '2018-10-08', 111, 7),
(31, '11', '2018-10-08', 11, 7),
(32, '1122', '2018-10-08', 333, 7),
(33, '1234', '2018-10-08', 555, 7),
(34, '123', '2018-10-08', 444, 7),
(35, '3', '2018-10-08', 34333, 7),
(36, '2233', '2018-10-08', 33, 7),
(37, '3232', '2018-10-08', 3, 7),
(38, '33', '2018-10-08', 33, 7),
(39, '12', '2018-10-08', 23, 7),
(40, '233', '2018-10-08', 44, 7),
(41, '45', '2018-10-08', 44, 7),
(42, '12', '2018-10-08', 22, 7),
(43, '122', '2018-10-08', 212, 7),
(44, '1234', '2018-10-09', 333, 7),
(45, '111', '2018-10-09', 1, 7),
(46, '44', '2018-10-09', 4, 7),
(47, '4', '2018-10-09', 4, 7),
(48, '111', '2018-10-09', 11, 7),
(49, '21', '2018-10-09', 121, 7),
(50, '22', '2018-10-09', 32, 7),
(51, '22', '2018-10-09', 32, 7),
(52, '33', '2018-10-10', 33, 7),
(53, '54678', '2018-10-10', 9, 7),
(54, '46464', '2018-10-10', 3434, 7),
(55, '46464', '2018-10-10', 3434, 7),
(56, '1234', '2018-10-12', 100, 7),
(57, '123456', '2018-10-12', 120, 7),
(58, '11', '2018-10-12', 150, 7),
(59, '123', '2018-10-13', 12, 7),
(60, '1236', '2018-10-13', 12, 7),
(61, '555', '2018-10-13', 3, 7),
(62, '123456', '2018-10-13', 12, 7),
(63, '12', '2018-10-13', 33, 7),
(64, '9984', '2018-10-14', 4599, 7),
(65, '11111', '2018-10-14', 11, 7),
(66, '11', '2018-10-14', 122, 7),
(67, '333', '2018-10-14', 33, 7),
(68, '1111122', '2018-10-14', 333, 7),
(69, '999', '2018-10-14', 78, 7),
(70, '2345', '2018-10-15', 333, 7),
(71, '1114444', '2018-10-16', 3444, 7),
(72, '1114444', '2018-10-16', 3444, 7),
(73, '1114444', '2018-10-16', 3444, 7),
(74, '1114444', '2018-10-16', 3444, 7),
(75, '1114444', '2018-10-16', 3444, 7),
(76, '1114444', '2018-10-16', 3444, 7),
(77, '1114444', '2018-10-16', 3444, 7),
(78, '1114444', '2018-10-16', 3444, 7),
(79, '1114444', '2018-10-16', 3444, 7),
(80, '1114444', '2018-10-16', 3444, 7),
(81, '1114444', '2018-10-16', 3444, 7),
(82, '1114444', '2018-10-16', 3444, 7),
(83, '1114444', '2018-10-16', 3444, 7),
(84, '1114444', '2018-10-16', 3444, 7),
(85, '1114444', '2018-10-16', 3444, 7),
(86, '1114444', '2018-10-16', 3444, 7),
(87, '1114444', '2018-10-16', 3444, 7),
(88, '1114444', '2018-10-16', 3444, 7),
(89, '1114444', '2018-10-16', 3444, 7),
(90, '1114444', '2018-10-16', 3444, 7),
(91, '1114444', '2018-10-16', 3444, 7),
(92, '1114444', '2018-10-16', 3444, 7),
(93, '1444', '2018-10-16', 444, 7),
(94, '1444', '2018-10-16', 444, 7),
(95, '4444', '2018-10-16', 444, 7),
(96, '4444', '2018-10-16', 444, 7),
(97, '4444', '2018-10-16', 444, 7),
(98, '4444', '2018-10-16', 444, 7),
(99, '222', '2018-10-16', 333, 7),
(100, '78', '2018-10-16', 555, 7),
(101, '888', '2018-10-17', 666, 7),
(102, '46', '2018-10-26', 8, 7),
(103, 'nbjm', '2018-10-31', 87, 7),
(104, '58785', '2018-10-31', 1000, 7),
(105, '4545', '2018-10-31', 666, 7),
(106, '3445', '2018-10-31', 44, 7),
(107, '3333', '2018-10-31', 3, 7),
(108, '2222', '2018-10-31', 2, 7),
(109, '22222', '2019-09-07', 120, 7);

-- --------------------------------------------------------

--
-- Estrutura para tabela `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(11) NOT NULL,
  `codigo_ibge` varchar(4) NOT NULL,
  `sigla` char(2) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `dtm_lcto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `estados`
--

INSERT INTO `estados` (`id_estado`, `codigo_ibge`, `sigla`, `nome`, `dtm_lcto`) VALUES
(1, '12', 'AC', 'Acre', '2018-10-05 16:55:35'),
(2, '27', 'AL', 'Alagoas', '2018-10-05 16:55:35'),
(3, '13', 'AM', 'Amazonas', '2018-10-05 16:55:35'),
(4, '16', 'AP', 'Amapá', '2018-10-05 16:55:35'),
(5, '29', 'BA', 'Bahia', '2018-10-05 16:55:35'),
(6, '23', 'CE', 'Ceará', '2018-10-05 16:55:35'),
(7, '53', 'DF', 'Distrito Federal', '2018-10-05 16:55:35'),
(8, '32', 'ES', 'Espírito Santo', '2018-10-05 16:55:35'),
(9, '52', 'GO', 'Goiás', '2018-10-05 16:55:35'),
(10, '21', 'MA', 'Maranhão', '2018-10-05 16:55:35'),
(11, '31', 'MG', 'Minas Gerais', '2018-10-05 16:55:35'),
(12, '50', 'MS', 'Mato Grosso do Sul', '2018-10-05 16:55:35'),
(13, '51', 'MT', 'Mato Grosso', '2018-10-05 16:55:35'),
(14, '15', 'PA', 'Pará', '2018-10-05 16:55:35'),
(15, '25', 'PB', 'Paraíba', '2018-10-05 16:55:35'),
(16, '26', 'PE', 'Pernambuco', '2018-10-05 16:55:35'),
(17, '22', 'PI', 'Piauí', '2018-10-05 16:55:35'),
(18, '41', 'PR', 'Paraná', '2018-10-05 16:55:35'),
(19, '33', 'RJ', 'Rio de Janeiro', '2018-10-05 16:55:35'),
(20, '24', 'RN', 'Rio Grande do Norte', '2018-10-05 16:55:35'),
(21, '11', 'RO', 'Rondônia', '2018-10-05 16:55:35'),
(22, '14', 'RR', 'Roraima', '2018-10-05 16:55:35'),
(23, '43', 'RS', 'Rio Grande do Sul', '2018-10-05 16:55:35'),
(24, '42', 'SC', 'Santa Catarina', '2018-10-05 16:55:35'),
(25, '28', 'SE', 'Sergipe', '2018-10-05 16:55:35'),
(26, '35', 'SP', 'São Paulo', '2018-10-05 16:55:35'),
(27, '17', 'TO', 'Tocantins', '2018-10-05 16:55:35');

-- --------------------------------------------------------

--
-- Estrutura para tabela `grupo_produtos`
--

CREATE TABLE `grupo_produtos` (
  `cd_grupo` int(11) NOT NULL,
  `gru_produto` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `grupo_produtos`
--

INSERT INTO `grupo_produtos` (`cd_grupo`, `gru_produto`) VALUES
(1, 'PERSIANAS'),
(2, 'TAPETES'),
(3, 'CORTINAS'),
(5, 'PAPEL DE PAREDE');

-- --------------------------------------------------------

--
-- Estrutura para tabela `itens_ajuste`
--

CREATE TABLE `itens_ajuste` (
  `cd_ajuest` int(11) DEFAULT NULL,
  `iaju_numitem` int(11) DEFAULT NULL,
  `iaju_codprod` int(11) DEFAULT NULL,
  `iaju_quantidade` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `itens_ajuste`
--

INSERT INTO `itens_ajuste` (`cd_ajuest`, `iaju_numitem`, `iaju_codprod`, `iaju_quantidade`) VALUES
(33, 4, 7, 2),
(33, 5, 5, 1),
(48, 2, 4, 1),
(52, 1, 1, 1),
(52, 2, 4, 1),
(52, 1, 6, 2),
(56, 1, 1, 1),
(56, 2, 4, 1),
(56, 1, 7, 1),
(65, 4, 1, 1),
(65, 4, 4, 1),
(65, 5, 6, 1),
(65, 7, 5, 1),
(74, 9, 4, 1),
(74, 10, 2, 1),
(74, 11, 5, 1),
(78, 13, 1, 1),
(78, 14, 1, 1),
(81, 1, 1, 1),
(81, 2, 2, 1),
(81, 1, 1, 1),
(81, 2, 7, 2),
(84, 16, 1, 1),
(84, 17, 4, 1),
(87, 19, 2, 1),
(87, 20, 6, 1),
(90, 23, 6, 1),
(90, 23, 6, 1),
(90, 23, 6, 1),
(90, 23, 6, 1),
(90, 23, 6, 1),
(90, 23, 6, 1),
(98, 26, 7, 1),
(101, 28, 6, 4),
(104, 30, 1, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `itens_entrada`
--

CREATE TABLE `itens_entrada` (
  `cd_itement` int(11) NOT NULL,
  `ient_cditem` int(11) DEFAULT NULL,
  `ient_produto` int(11) DEFAULT NULL,
  `ient_quantidade` int(11) DEFAULT NULL,
  `ient_vcusto` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `itens_entrada`
--

INSERT INTO `itens_entrada` (`cd_itement`, `ient_cditem`, `ient_produto`, `ient_quantidade`, `ient_vcusto`) VALUES
(1, 1, 1, 1, 1),
(31, 1, 1, 1, 1),
(31, 1, 6, 2, 45),
(31, 2, 4, 4, 590),
(31, 3, 4, 3, 33),
(32, 1, 2, 1, 56),
(33, 1, 4, 3, 56),
(34, 1, 5, 3, 689),
(35, 1, 7, 2, 444),
(36, 1, 2, 2, 45),
(37, 1, 1, 2, 3),
(38, 1, 1, 2, 44),
(38, 1, 1, 2, 44),
(38, 1, 1, 2, 44),
(38, 2, 1, 1, 2),
(38, 2, 1, 1, 2),
(38, 2, 1, 1, 2),
(39, 1, 1, 2, 34),
(40, 1, 2, 1, 44),
(41, 1, 1, 4, 4),
(42, 1, 1, 3, 44),
(43, 1, 6, 2, 33),
(44, 1, 2, 1, 34),
(45, 1, 1, 1, 680),
(46, 1, 4, 1, 44),
(47, 1, 1, 2, 2),
(48, 1, 1, 1, 33),
(48, 2, 4, 3, 4),
(51, 1, 1, 1, 12),
(53, 1, 4, 1, 565),
(53, 2, 1, 1, 1),
(55, 1, 2, 1, 1),
(55, 2, 4, 3, 3),
(56, 1, 1, 1, 100),
(57, 1, 4, 1, 120),
(58, 1, 2, 1, 140),
(59, 1, 1, 1, 12),
(60, 1, 2, 1, 12),
(61, 1, 5, 12, 12),
(62, 1, 2, 1, 12),
(63, 1, 7, 1, 33),
(64, 1, 2, 1, 1400),
(64, 2, 4, 1, 980),
(64, 3, 6, 1, 1350),
(65, 1, 1, 1, 11),
(65, 2, 1, 1, 11),
(66, 1, 1, 1, 11),
(67, 1, 2, 1, 33),
(68, 1, 4, 1, 33),
(69, 1, 4, 1, 87),
(70, 1, 2, 1, 333),
(92, 1, 4, 5, 34),
(94, 1, 1, 5, 11),
(98, 1, 2, 2, 35),
(98, 2, 4, 2, 55),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(99, 1, 1, 1, 444),
(100, 1, 2, 2, 22),
(101, 1, 1, 1, 444),
(103, 1, 2, 1, 87),
(104, 1, 2, 1, 1000),
(105, 1, 1, 2, 450),
(106, 1, 1, 1, 333),
(107, 1, 6, 3, 45),
(109, 1, 6, 1, 120);

-- --------------------------------------------------------

--
-- Estrutura para tabela `log_geral`
--

CREATE TABLE `log_geral` (
  `cd_log` int(11) NOT NULL,
  `log_data` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_rotina` varchar(10) DEFAULT NULL,
  `log_codrotina` int(11) DEFAULT NULL,
  `log_descricao` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `movimentacao_produtos`
--

CREATE TABLE `movimentacao_produtos` (
  `cd_mov` int(11) NOT NULL,
  `mov_data` date DEFAULT NULL,
  `mov_rotina` varchar(5) DEFAULT NULL,
  `mov_es` varchar(2) DEFAULT NULL,
  `mov_produto` int(11) DEFAULT NULL,
  `mov_nota` int(11) DEFAULT NULL,
  `mov_quant` int(11) DEFAULT NULL,
  `mov_valorunit` decimal(10,2) DEFAULT NULL,
  `mov_valortotal` decimal(10,2) DEFAULT NULL,
  `mov_obs` text,
  `mov_codcontrole` int(11) DEFAULT NULL,
  `mov_item` int(11) DEFAULT NULL,
  `mov_clientefornec` int(11) DEFAULT NULL,
  `mov_nomerazao` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura para tabela `produtos`
--

CREATE TABLE `produtos` (
  `cd_produto` int(11) NOT NULL,
  `pro_datacad` date DEFAULT NULL,
  `pro_descricao` varchar(255) DEFAULT NULL,
  `pro_grupo` int(11) DEFAULT NULL,
  `pro_codbarras` text,
  `pro_unidade` int(11) DEFAULT NULL,
  `pro_aces1` text,
  `pro_aces2` text,
  `pro_aces3` text,
  `pro_aces4` text,
  `pro_aces5` text,
  `pro_aces6` text,
  `pro_aces7` text,
  `pro_aces8` text,
  `pro_aces9` text,
  `pro_aces10` text,
  `pro_altura` text,
  `pro_largura` text,
  `pro_precocusto` decimal(10,2) DEFAULT NULL,
  `pro_customed` decimal(10,2) DEFAULT NULL,
  `pro_precovenda` decimal(10,2) DEFAULT NULL,
  `pro_estoquemin` int(11) DEFAULT NULL,
  `pro_estoquemax` int(11) DEFAULT NULL,
  `pro_estoque` int(11) DEFAULT '0',
  `pro_obs` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `produtos`
--

INSERT INTO `produtos` (`cd_produto`, `pro_datacad`, `pro_descricao`, `pro_grupo`, `pro_codbarras`, `pro_unidade`, `pro_aces1`, `pro_aces2`, `pro_aces3`, `pro_aces4`, `pro_aces5`, `pro_aces6`, `pro_aces7`, `pro_aces8`, `pro_aces9`, `pro_aces10`, `pro_altura`, `pro_largura`, `pro_precocusto`, `pro_customed`, `pro_precovenda`, `pro_estoquemin`, `pro_estoquemax`, `pro_estoque`, `pro_obs`) VALUES
(1, '2018-10-06', 'TAPETE PERSA AZUL', 2, '8383819', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '280', '690', '333.00', '337.58', '1500.00', 2, 10, 0, ''),
(2, '2018-10-04', 'tapete indiano', 2, '2193234328', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1000', '1440', '1000.00', '548.89', '1800.00', 1, 10, 0, 'teste'),
(4, '2018-10-07', 'PAPEL DE PAREDE ROSA', 5, '324234234', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10', '6', '55.00', '83.72', '98.00', 20, 100, 0, ''),
(5, '2018-10-07', 'CORTINA DO DAVID ', 3, '2934304920', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1000', '500', '12.00', '6.00', '980.00', 2, 10, 0, ''),
(6, '2018-10-07', 'CORTINA VOAL', 3, '2323132', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '100', '500', '120.00', '240.00', '140.00', 2, 10, 0, ''),
(7, '2018-10-07', 'CORTINA VOALll', 3, '9393939', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '100', '400', '33.00', '16.50', '150.00', 1, 10, 0, '');

-- --------------------------------------------------------

--
-- Estrutura para tabela `unidade_produto`
--

CREATE TABLE `unidade_produto` (
  `cd_unidade` int(11) NOT NULL,
  `uni_sigla` text,
  `uni_descricao` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Despejando dados para a tabela `unidade_produto`
--

INSERT INTO `unidade_produto` (`cd_unidade`, `uni_sigla`, `uni_descricao`) VALUES
(1, 'PC', 'PEÇA'),
(2, 'UN', 'UNIDADE'),
(3, 'JG', 'JOGO'),
(4, 'KT', 'KIT'),
(5, 'MT', 'METRO');

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `usuario_id` int(5) NOT NULL,
  `nome` text COLLATE latin1_general_ci NOT NULL,
  `sobrenome` varchar(50) COLLATE latin1_general_ci DEFAULT '',
  `data_nasc` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `cpf` varchar(15) COLLATE latin1_general_ci DEFAULT NULL,
  `rg` varchar(10) COLLATE latin1_general_ci DEFAULT NULL,
  `endereco` text COLLATE latin1_general_ci,
  `numero` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `bairro` text COLLATE latin1_general_ci,
  `complemento` text COLLATE latin1_general_ci,
  `cidade` text COLLATE latin1_general_ci,
  `estado` varchar(5) COLLATE latin1_general_ci DEFAULT NULL,
  `cep` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `fone` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `email` varchar(100) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `usuario` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `senha` varchar(255) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `info` text COLLATE latin1_general_ci,
  `nivel_usuario` enum('0','1','2') COLLATE latin1_general_ci NOT NULL DEFAULT '0',
  `data_cadastro` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `data_ultimo_login` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `ativado` enum('0','1') COLLATE latin1_general_ci NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Despejando dados para a tabela `usuarios`
--

INSERT INTO `usuarios` (`usuario_id`, `nome`, `sobrenome`, `data_nasc`, `cpf`, `rg`, `endereco`, `numero`, `bairro`, `complemento`, `cidade`, `estado`, `cep`, `fone`, `email`, `usuario`, `senha`, `info`, `nivel_usuario`, `data_cadastro`, `data_ultimo_login`, `ativado`) VALUES
(1, 'Admin', 'admin', '1607299200', '222', '', 'Rua Augusto Sacratin', '1221', 'Vila Omar', 'sdsds', 'Americana', '2', '13469097', '19191919', 'wewewe', 'admin', '827ccb0eea8a706c4c34a16891f84e7b', '4444', '2', NULL, NULL, '1'),
(14, 'Roger', 'Murbach', NULL, NULL, NULL, '', '', '', NULL, NULL, NULL, NULL, NULL, 'rogermf@msn.com', 'roger', '827ccb0eea8a706c4c34a16891f84e7b', '', '2', NULL, '2020-12-07 09:10:55', '1');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `ajuste_estoque`
--
ALTER TABLE `ajuste_estoque`
  ADD PRIMARY KEY (`cd_ajuste`);

--
-- Índices de tabela `cad_clientes`
--
ALTER TABLE `cad_clientes`
  ADD PRIMARY KEY (`cd_cliente`);

--
-- Índices de tabela `cad_empresa`
--
ALTER TABLE `cad_empresa`
  ADD PRIMARY KEY (`cd_empresa`);

--
-- Índices de tabela `cad_fornecedores`
--
ALTER TABLE `cad_fornecedores`
  ADD PRIMARY KEY (`cd_fornecedor`);

--
-- Índices de tabela `cond_pagamento`
--
ALTER TABLE `cond_pagamento`
  ADD PRIMARY KEY (`cd_pagamento`);

--
-- Índices de tabela `contas_pagar`
--
ALTER TABLE `contas_pagar`
  ADD PRIMARY KEY (`cd_conpag`);

--
-- Índices de tabela `entrada_produto`
--
ALTER TABLE `entrada_produto`
  ADD PRIMARY KEY (`cd_entrada`);

--
-- Índices de tabela `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`);

--
-- Índices de tabela `grupo_produtos`
--
ALTER TABLE `grupo_produtos`
  ADD PRIMARY KEY (`cd_grupo`);

--
-- Índices de tabela `log_geral`
--
ALTER TABLE `log_geral`
  ADD PRIMARY KEY (`cd_log`);

--
-- Índices de tabela `movimentacao_produtos`
--
ALTER TABLE `movimentacao_produtos`
  ADD PRIMARY KEY (`cd_mov`);

--
-- Índices de tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`cd_produto`);

--
-- Índices de tabela `unidade_produto`
--
ALTER TABLE `unidade_produto`
  ADD PRIMARY KEY (`cd_unidade`);

--
-- Índices de tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuario_id`),
  ADD UNIQUE KEY `usuario` (`usuario`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `ajuste_estoque`
--
ALTER TABLE `ajuste_estoque`
  MODIFY `cd_ajuste` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT de tabela `cad_clientes`
--
ALTER TABLE `cad_clientes`
  MODIFY `cd_cliente` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `cad_empresa`
--
ALTER TABLE `cad_empresa`
  MODIFY `cd_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `cad_fornecedores`
--
ALTER TABLE `cad_fornecedores`
  MODIFY `cd_fornecedor` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `cond_pagamento`
--
ALTER TABLE `cond_pagamento`
  MODIFY `cd_pagamento` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `contas_pagar`
--
ALTER TABLE `contas_pagar`
  MODIFY `cd_conpag` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1063473;

--
-- AUTO_INCREMENT de tabela `entrada_produto`
--
ALTER TABLE `entrada_produto`
  MODIFY `cd_entrada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT de tabela `estados`
--
ALTER TABLE `estados`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT de tabela `grupo_produtos`
--
ALTER TABLE `grupo_produtos`
  MODIFY `cd_grupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `log_geral`
--
ALTER TABLE `log_geral`
  MODIFY `cd_log` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de tabela `movimentacao_produtos`
--
ALTER TABLE `movimentacao_produtos`
  MODIFY `cd_mov` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `cd_produto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de tabela `unidade_produto`
--
ALTER TABLE `unidade_produto`
  MODIFY `cd_unidade` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuario_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
