<?php

define('DB_SERVER', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', 'root');
define('DB_NAME', 'painel2');


if (isset($_GET['term'])){
    $return_arr = array();

    try {
        $conn = new PDO("mysql:host=".DB_SERVER.";port=3306;dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        //$stmt = $conn->prepare('SELECT CONCAT(cd_produto,\' \',pro_descricao) as produtos FROM produtos where pro_descricao LIKE :term');
        $stmt = $conn->prepare('SELECT pro_descricao FROM produtos WHERE pro_descricao LIKE :term');
        $stmt->execute(array('term' => '%'.$_GET['term'].'%'));

        while($row = $stmt->fetch()) {
            $results[] = array('label' => $row['cd_produto'],'cpf' => $row['pro_descricao']);

            $return_arr[] = $row['pro_descricao'];
        }

    } catch(PDOException $e) {
        echo 'ERROR: ' . $e->getMessage();
    }


    /* Toss back results as json encoded array. */
    echo json_encode($return_arr);
}


?>
