<?php 

include_once ("config.php");

?>

<div>
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-button">
        <div class="container"><a class="navbar-brand" href="<?php echo $path ?>/painel.php" >KAZA W</a><button class="navbar-toggler" data-toggle="collapse" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"
                 id="navcol-1">
                <ul class="nav navbar-nav mr-auto">
                    <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Vendas&nbsp;</a>
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Nova Venda</a><a class="dropdown-item" role="presentation" href="#">Orçamentos</a><a class="dropdown-item" role="presentation" href="#">Consultar vendas Efetuadas</a></div>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Cadastros&nbsp;</a>
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/cliente/consulta_cliente.php">Clientes</a><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/fornecedor/consulta_fornecedor.php">Fornecedores</a>
                            <div class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="#">Grupo de Clientes</a></div>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Estoque&nbsp;</a>
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/estoque/produtos.php">Cadastrar Produtos</a>
                            <div class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/estoque/entrada_produto.php">Entrada de Produtos (+)</a><a class="dropdown-item" role="presentation" href="#">Consultar Entrada de Produtos</a>
                            <div class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="#">Saída de Produtos (-)</a><a class="dropdown-item" role="presentation" href="#">Consultar Saída de Produtos</a>
                            <div class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/estoque/ajuste_estoque.php">Ajuste de Estoque</a>
                            <div class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/geral/grupo_produtos.php">Grupo de Produtos</a><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/geral/unidade_produtos.php">Unidade de Produtos</a></div>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Receber&nbsp;</a>
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Contas a Receber</a><a class="dropdown-item" role="presentation" href="#">Contas Recebidas</a>
                            <div class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="#">Gerar Recibo</a></div>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Pagar&nbsp;</a>
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Contas a Pagar</a><a class="dropdown-item" role="presentation" href="#">Contas Pagas</a>
                            <div class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="#">Categorias</a></div>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#">Outros&nbsp;</a>
                        <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Fluxo de Caixa</a><a class="dropdown-item" role="presentation" href="#">Parâmetros</a><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/financeiro/formas_pagamento.php">Cadastro de Formas Pagto/Recto</a>
                            <div
                                class="dropdown-divider" role="presentation"></div><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/usuarios/consulta_usuario.php">Usuários</a><a class="dropdown-item" role="presentation" href="<?php echo $path ?>/pages/geral/historico_acoes.php">Histórico de Ações</a><a class="dropdown-item" role="presentation" href="#">Suporte</a></div>
                    </li>
                </ul><span class="navbar-text actions"> <a class="btn btn-light action-button" role="button" href="<?php echo $path ?>/sair.php">Sair</a></span></div>
        </div>
    </nav>
</div>

<script src="<?php echo $path ?>/assets/js/jquery.min.js"></script>
<script src="<?php echo $path ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $path ?>/assets/js/Profile-Edit-Form.js"></script>
<script src="<?php echo $path ?>/assets/bootstrap/js/bootbox.min.js"></script>
<script src="<?php echo $path ?>/assets/bootstrap/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo $path ?>/assets/bootstrap/js/bootstrap-datepicker.pt-BR.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
