<?php
session_start();
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sis_kazaW</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <div class="login-clean">

        <form action="verifica_usuario.php" method="post">
            <h2 class="sr-only">Login Form</h2>
            <?php
              if (isset($_SESSION['msg'])){
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
              }
              if (isset($_SESSION['msgcad'])){
                echo $_SESSION['msgcad'];
                unset($_SESSION['msgcad']);
              }
            ?>
            <div class="illustration"><img src="assets/img/logo.png"></div>
            <div class="form-group"><input class="form-control" type="text" name="usuario" id="usuario" placeholder="Usuario"></div>
            <div class="form-group"><input class="form-control" type="password" name="senha" id="senha" placeholder="Senha"></div>
            <div class="form-group"><button class="btn btn-primary btn-block" type="submit" name="btnLogin">Entrar</button></div><a href="#" class="forgot">Recuperar Senha</a>
            <div class="form-group"><label class="demo"> Demo: admin / 12345 </label></div></form>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <style type="text/css">
    .btn-primary {
      color: #fff;
      background-color: #73a5e2 !important;
    }
    </style>
</body>

</html>
